use day20::*;
use std::time::Instant;

const KEY: i64 = 811589153;

fn main() {
    let (nums, mut indices) = {
        let (mut nums, indices) = build_data();
        nums.iter_mut().for_each(|n| *n *= KEY);
        (nums, indices)
    };
    let start = Instant::now();
    mix_n(&nums, &mut indices, 10);
    let end = Instant::now();
    let (x, y, z) = find_coords(&nums, &indices);
    println!("Solution: {}", x + y + z);
    println!("Elapsed: {:?}", end - start);
}
