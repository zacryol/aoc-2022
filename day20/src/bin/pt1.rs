use day20::*;
use std::time::Instant;

fn main() {
    let (nums, mut indices) = build_data();
    let start = Instant::now();
    mix_n(&nums, &mut indices, 1);
    let end = Instant::now();
    let (x, y, z) = find_coords(&nums, &indices);
    println!("Solution: {}", x + y + z);
    println!("Elapsed: {:?}", end - start);
}
