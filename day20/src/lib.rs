use std::{
    cmp::Ordering,
    collections::{HashMap, VecDeque},
    fs,
};

#[derive(Debug)]
pub struct MultiDeque {
    data: Vec<VecDeque<i64>>,
    cache: HashMap<i64, usize>,
}

impl MultiDeque {
    // Note: implementation of MultiDeque most likely WILL NOT WORK if Buckets happen to be
    // unevenly sized. With an input file of 5000 numbers, this is fine, but doesn't work for the
    // sample of 7 nums unless this value here is changed to 1 or 7
    // Note 2: bucket size of 250 was found to be optimal for a total size of 5000
    const BUCKET_SIZE: usize = 250;

    pub fn len(&self) -> usize {
        self.data.iter().map(|vd| vd.len()).sum()
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    pub fn get_num(&self, idx: usize) -> i64 {
        let vdi = idx / Self::BUCKET_SIZE;
        let in_vd = idx % Self::BUCKET_SIZE;
        self.data[vdi][in_vd]
    }

    fn find_vdi(&self, value: i64) -> usize {
        self.cache[&value]
    }

    pub fn find_idx(&self, value: i64) -> usize {
        let vdi = self.find_vdi(value);

        let in_vd = self.data[vdi]
            .iter()
            .enumerate()
            .find(|(_i, n)| **n == value)
            .unwrap()
            .0;
        vdi * Self::BUCKET_SIZE + in_vd
    }

    pub fn move_by(&mut self, idx: usize, by: i64) {
        if by == 0 {
            return;
        }
        let l = self.len() as i64;
        let new_idx = {
            let new_idx = ((idx as i64) + by) % (l - 1);
            let new_idx = (if new_idx > l {
                new_idx % l - 1
            } else if new_idx < 0 {
                new_idx + l - 1
            } else {
                new_idx
            } % l) as usize;
            if new_idx == 0 {
                (l - 1) as usize
            } else {
                new_idx
            }
        };
        match new_idx.cmp(&idx) {
            Ordering::Greater => {
                let start_vec_id = idx / Self::BUCKET_SIZE;
                let end_vec_id = new_idx / Self::BUCKET_SIZE;

                // if just within one vd
                if start_vec_id == end_vec_id {
                    let idxx1 = idx % Self::BUCKET_SIZE;
                    let idxx2 = new_idx % Self::BUCKET_SIZE;
                    let n = self.data[end_vec_id].remove(idxx1).unwrap();
                    self.data[end_vec_id].insert(idxx2, n);
                    return;
                }

                // remove from start vd
                let num = self.data[start_vec_id]
                    .remove(idx % Self::BUCKET_SIZE)
                    .unwrap();

                // Cascade backwards
                for vdi in start_vec_id..end_vec_id {
                    let n = self.data[vdi + 1].pop_front().unwrap();
                    self.data[vdi].push_back(n);
                    *self.cache.get_mut(&n).unwrap() = vdi;
                }

                // insert at end
                self.data[end_vec_id].insert(new_idx % Self::BUCKET_SIZE, num);
                *self.cache.get_mut(&num).unwrap() = end_vec_id;
            }
            Ordering::Less => {
                let from_vdi = idx / Self::BUCKET_SIZE; // larger value
                let to_vdi = new_idx / Self::BUCKET_SIZE; // smaller value

                // remove from from vd
                let num = self.data[from_vdi].remove(idx % Self::BUCKET_SIZE).unwrap();

                // Cascade forwards
                for vdi in to_vdi..from_vdi {
                    let n = self.data[vdi].pop_back().unwrap();
                    self.data[vdi + 1].push_front(n);
                    *self.cache.get_mut(&n).unwrap() = vdi + 1;
                }

                // insert at to vdi
                self.data[to_vdi].insert(new_idx % Self::BUCKET_SIZE, num);
                *self.cache.get_mut(&num).unwrap() = to_vdi;
            }
            Ordering::Equal => (), // do nothing
        }
    }
}

// The collect is NOT needless as values need to be pulled from v_iter first before draining the
// rest of them into last_vd
#[allow(clippy::needless_collect)]
impl From<Vec<i64>> for MultiDeque {
    fn from(value: Vec<i64>) -> Self {
        let len = value.len() / Self::BUCKET_SIZE;
        let each = value.len() / len;
        let mut cache = HashMap::with_capacity(value.len());
        let mut v_iter = value.into_iter();

        let data = (0..(len - 1))
            .map(|i| {
                let mut vd = VecDeque::with_capacity(each + 2);
                for _ in 0..each {
                    let num = v_iter.next().unwrap();
                    vd.push_back(num);
                    cache.insert(num, i);
                }
                vd
            })
            .collect::<Vec<_>>();
        let last_vd = v_iter
            .inspect(|num| {
                cache.insert(*num, len - 1);
            })
            .collect::<VecDeque<_>>();
        let data = data.into_iter().chain([last_vd]).collect();
        Self { data, cache }
    }
}

pub fn read_data() -> Vec<i64> {
    let text = fs::read_to_string("inputs/day20.txt").unwrap();
    text.lines().filter_map(|l| l.parse::<i64>().ok()).collect()
}

pub fn build_data() -> (Vec<i64>, MultiDeque) {
    let nums = read_data();
    let indices = MultiDeque::from((0i64..(nums.len() as i64)).collect::<Vec<_>>());
    (nums, indices)
}

pub fn do_mixing(nums: &[i64], indices: &mut MultiDeque) {
    for (i, n) in nums.iter().enumerate() {
        let idx = indices.find_idx(i as i64);
        indices.move_by(idx, *n)
    }
}

pub fn mix_n(nums: &[i64], indices: &mut MultiDeque, times: usize) {
    for _ in 0..times {
        do_mixing(nums, indices);
    }
}

pub fn find_coords(nums: &[i64], indices: &MultiDeque) -> (i64, i64, i64) {
    let base_idx =
        indices.find_idx(nums.iter().enumerate().find(|(_i, n)| **n == 0).unwrap().0 as i64);
    let (a, b, c) = (
        (base_idx + 1000) % (indices.len()),
        (base_idx + 2000) % (indices.len()),
        (base_idx + 3000) % (indices.len()),
    );
    (
        nums[indices.get_num(a) as usize],
        nums[indices.get_num(b) as usize],
        nums[indices.get_num(c) as usize],
    )
}
