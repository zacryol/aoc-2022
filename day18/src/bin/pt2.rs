use day18::*;

fn main() {
    let faces = get_all_faces();

    println!("{}", only_outside_faces(&unique_faces(&faces)).len());
}
