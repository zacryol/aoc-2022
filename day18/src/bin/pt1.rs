use day18::*;

fn main() {
    let faces = get_all_faces();

    println!("{}", unique_faces(&faces).len());
}
