use nom::{bytes::complete::tag, character::complete::u8 as nomu8, Finish, IResult};
use pathfinding::prelude::bfs;
use rayon::prelude::*;
use std::fs;

pub fn read_data() -> Vec<Cube> {
    let text = fs::read_to_string("inputs/day18.txt").unwrap();
    text.lines()
        .filter_map(|l| cube(l).finish().ok().map(|(_input, c)| c))
        .collect()
}

// x, y, z coords
type Cube = (u8, u8, u8);
type Face = (u8, u8, u8);

fn cube(input: &str) -> IResult<&str, Cube> {
    let (input, x) = nomu8(input)?;
    let (input, _) = tag(",")(input)?;
    let (input, y) = nomu8(input)?;
    let (input, _) = tag(",")(input)?;
    let (input, z) = nomu8(input)?;
    Ok((input, (x, y, z)))
}

pub fn get_all_faces() -> Vec<Face> {
    let cubes = read_data();
    let mut faces: Vec<_> = cubes.into_iter().flat_map(cube_to_faces).collect();
    faces.sort();
    faces
}

// faces should be sorted
pub fn only_outside_faces<'face>(faces: &'face [&Face]) -> Vec<&'face Face> {
    let max_coord = {
        let c = faces.iter().fold((0, 0, 0), |(x1, y1, z1), (x2, y2, z2)| {
            (x1.max(*x2), y1.max(*y2), z1.max(*z2))
        });
        // snap to even, add buffer space
        (
            c.0 + (c.0 % 2) + 2,
            c.1 + (c.1 % 2) + 2,
            c.2 + (c.2 % 2) + 2,
        )
    };

    faces
        //.into_iter()
        .par_iter()
        .filter(|f| {
            bfs(
                **f,
                |p| match p {
                    (x, y, z) if x % 2 == 1 => {
                        assert_eq!(y % 2, 0);
                        assert_eq!(z % 2, 0);
                        vec![(x + 1, *y, *z), (x - 1, *y, *z)]
                    }
                    (x, y, z) if y % 2 == 1 => vec![(*x, y + 1, *z), (*x, y - 1, *z)],
                    (x, y, z) if z % 2 == 1 => vec![(*x, *y, z + 1), (*x, *y, z - 1)],
                    (x, y, z) => {
                        let xd = x
                            .checked_sub(2)
                            .filter(|_| faces.binary_search(&&(x - 1, *y, *z)).is_err())
                            .unwrap_or(*x);
                        let yd = y
                            .checked_sub(2)
                            .filter(|_| faces.binary_search(&&(*x, y - 1, *z)).is_err())
                            .unwrap_or(*y);
                        let zd = z
                            .checked_sub(2)
                            .filter(|_| faces.binary_search(&&(*x, *y, z - 1)).is_err())
                            .unwrap_or(*z);
                        let xu = Some((x + 2).min(max_coord.0))
                            .filter(|_| faces.binary_search(&&(x + 1, *y, *z)).is_err())
                            .unwrap_or(*x);
                        let yu = Some((y + 2).min(max_coord.1))
                            .filter(|_| faces.binary_search(&&(*x, y + 1, *z)).is_err())
                            .unwrap_or(*y);
                        let zu = Some((z + 2).min(max_coord.2))
                            .filter(|_| faces.binary_search(&&(*x, *y, z + 1)).is_err())
                            .unwrap_or(*z);
                        vec![
                            (xd, *y, *z),
                            (xu, *y, *z),
                            (*x, yd, *z),
                            (*x, yu, *z),
                            (*x, *y, zd),
                            (*x, *y, zu),
                        ]
                    }
                },
                |p| *p == (0, 0, 0),
            )
            .is_some()
        })
        .copied()
        .collect()
}

pub fn cube_to_faces(c: Cube) -> [Face; 6] {
    let t = (c.0 * 2, c.1 * 2, c.2 * 2);
    [
        (t.0 + 1, t.1, t.2),
        (t.0 - 1, t.1, t.2),
        (t.0, t.1 + 1, t.2),
        (t.0, t.1 - 1, t.2),
        (t.0, t.1, t.2 + 1),
        (t.0, t.1, t.2 - 1),
    ]
}

// faces should be sorted
pub fn unique_faces(faces: &[Face]) -> Vec<&Face> {
    let mut unique_faces = vec![];
    // Check first face
    if faces[0] != faces[1] {
        unique_faces.push(&faces[0]);
    }
    // check other faces
    for i in 0..(faces.len() - 2) {
        let fa = &faces[i];
        let fb = &faces[i + 1];
        let fc = &faces[i + 2];

        if fb != fa && fb != fc {
            unique_faces.push(fb);
        }
    }
    // check last face
    let l = faces.len();
    if faces[l - 1] != faces[l - 2] {
        unique_faces.push(&faces[l - 1]);
    }
    unique_faces
}
