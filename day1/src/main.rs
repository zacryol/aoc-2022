use std::fs;

fn main() {
    let data = fs::read_to_string("inputs/day1.txt").unwrap();
    let lines = data.lines().map(|s| s.parse::<usize>().ok());
    let mut sums: Vec<usize> = vec![];

    let mut current_sum = 0;
    for c in lines {
        if let Some(c) = c {
            current_sum += c;
        } else {
            sums.push(current_sum);
            current_sum = 0;
        }
    }
    let mut top3 = [0, 0, 0];
    for i in sums.iter() {
        let min = top3.iter_mut().min().unwrap();
        if i > min {
            *min = *i;
        }
    }
    println!("{:?}", top3.iter().sum::<usize>());
}
