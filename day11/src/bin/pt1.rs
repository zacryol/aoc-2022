use day11::*;

fn main() {
    let mut monkeys: Vec<_> = read_data();
    for _i in 0..20 {
        go_round(&mut monkeys, 3);
    }
    monkeys.sort_by_key(|m| usize::MAX - m.get_inspection_count());
    println!("{:?}", get_monkey_business(&monkeys));
}
