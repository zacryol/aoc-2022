use std::fs;
use std::{cell::RefCell, collections::VecDeque};

pub fn read_data() -> Vec<Monkey> {
    let text = fs::read_to_string("inputs/day11.txt").unwrap();
    let lines = text.lines().collect::<Vec<&str>>();
    let mut monkeys = lines.chunks(7).map(Monkey::from).collect::<Vec<_>>();
    let magic_number = monkeys.iter().map(|m| m.test_div).product::<u128>();
    monkeys
        .iter_mut()
        .for_each(|m| m.magic_number = Some(magic_number));

    monkeys
}

pub fn go_round(monkeys: &mut Vec<Monkey>, relief: u128) {
    for m in 0..monkeys.len() {
        for (item, target) in monkeys[m].inspect_and_throw(relief) {
            monkeys
                .iter_mut()
                .find(|mm| mm.id == target)
                .unwrap()
                .add_item(item)
        }
    }
}

pub fn get_monkey_business(monkeys: &[Monkey]) -> usize {
    monkeys
        .iter()
        .take(2)
        .map(|m| m.get_inspection_count())
        .product::<usize>()
}

#[derive(Debug)]
pub enum OpType {
    Mul,
    Add,
}

#[derive(Debug)]
pub enum OpRHS {
    Num(u128),
    Old,
}

#[derive(Debug)]
pub struct Monkey {
    pub id: usize,
    items: VecDeque<u128>,
    op: (OpType, OpRHS),
    test_div: u128,
    target: (usize, usize),
    inspection_count: RefCell<usize>,
    magic_number: Option<u128>, // Chinese Remainder Theorem whatever.
}

impl Monkey {
    pub fn add_item(&mut self, item: u128) {
        self.items.push_back(item)
    }

    pub fn get_inspection_count(&self) -> usize {
        *self.inspection_count.borrow()
    }

    pub fn inspect_and_throw(&mut self, relief: u128) -> Vec<(u128, usize)> {
        self.items
            .drain(..)
            .collect::<Vec<_>>()
            .into_iter()
            .map(|i| self.inspect_item(i) / relief)
            .map(|i| {
                (
                    i,
                    if i % self.test_div == 0 {
                        self.target.0
                    } else {
                        self.target.1
                    },
                )
            })
            .collect()
    }

    fn inspect_item(&self, item: u128) -> u128 {
        *self.inspection_count.borrow_mut() += 1;
        let val_by = match self.op.1 {
            OpRHS::Old => item,
            OpRHS::Num(n) => n,
        };
        (match self.op.0 {
            OpType::Mul => item * val_by,
            OpType::Add => item + val_by,
        }) % self.magic_number.unwrap()
    }
}

impl From<&[&str]> for Monkey {
    fn from(s: &[&str]) -> Self {
        let id: usize = s[0]
            .trim()
            .strip_prefix("Monkey ")
            .unwrap()
            .strip_suffix(':')
            .unwrap()
            .parse()
            .unwrap();
        let items = s[1]
            .trim()
            .strip_prefix("Starting items: ")
            .unwrap()
            .split(", ")
            .map(|s| s.parse().unwrap())
            .collect();
        let op = {
            let data = s[2]
                .trim()
                .strip_prefix("Operation: new = old ")
                .unwrap()
                .split_once(' ')
                .unwrap();
            (
                match data.0 {
                    "+" => OpType::Add,
                    "*" => OpType::Mul,
                    _ => panic!(),
                },
                match data.1 {
                    "old" => OpRHS::Old,
                    n => OpRHS::Num(n.parse().unwrap()),
                },
            )
        };
        let test_div = s[3]
            .trim()
            .strip_prefix("Test: divisible by ")
            .unwrap()
            .parse()
            .unwrap();
        let target: (usize, usize) = (
            s[4].trim()
                .strip_prefix("If true: throw to monkey ")
                .unwrap()
                .parse()
                .unwrap(),
            s[5].trim()
                .strip_prefix("If false: throw to monkey ")
                .unwrap()
                .parse()
                .unwrap(),
        );
        Monkey {
            id,
            items,
            op,
            test_div,
            target,
            inspection_count: RefCell::new(0),
            magic_number: None,
        }
    }
}
