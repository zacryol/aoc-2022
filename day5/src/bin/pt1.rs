fn main() {
    let text = std::fs::read_to_string("inputs/day5.txt").unwrap();
    let mut lines = text.lines();
    let mut stacks = day5::build_stacks_init(&mut lines);
    lines
        .filter(|l| !l.is_empty())
        .map(|l| {
            l.split(' ')
                .filter_map(|i| i.parse::<usize>().ok())
                .collect::<Vec<_>>()
        })
        .for_each(|l| day5::move_items_1(&mut stacks, l[0], l[1] - 1, l[2] - 1));
    println!(
        "{}",
        stacks.iter().map(|s| s.last().unwrap()).collect::<String>()
    );
}
