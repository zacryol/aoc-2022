use std::{collections::VecDeque, str::Lines};

pub fn build_stacks_init(lines: &mut Lines) -> Vec<Vec<char>> {
    let mut initial_stack_lines = Vec::<&str>::new();
    let _row_counter = loop {
        let line = lines.next().unwrap();
        if line.starts_with('[') {
            initial_stack_lines.push(line);
            continue;
        }
        break line;
    };
    let initial_stack_lines = initial_stack_lines
        .into_iter()
        .map(|l| {
            l.as_bytes()
                .chunks(4)
                .map(|s| s[1] as char)
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>();
    (0..(initial_stack_lines[0].len()))
        .map(|i| {
            initial_stack_lines
                .iter()
                .rev()
                .map(|l| l[i])
                .filter(|c| c.is_alphabetic())
                .collect::<Vec<_>>()
        })
        .collect::<Vec<Vec<char>>>()
}

pub fn move_items_1(stacks: &mut [Vec<char>], amount: usize, from: usize, to: usize) {
    for _ in 0..amount {
        let num = stacks[from].pop().unwrap();
        stacks[to].push(num);
    }
}

pub fn move_items_2(stacks: &mut [Vec<char>], amount: usize, from: usize, to: usize) {
    let mut temp = VecDeque::new();
    for _ in 0..amount {
        let num = stacks[from].pop().unwrap();
        temp.push_front(num);
    }
    temp.into_iter().for_each(|c| stacks[to].push(c));
}
