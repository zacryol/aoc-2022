use day19::*;

fn main() {
    let blueprints = read_data();
    //println!("{:?}", blueprints);

    //println!("{:?}", simulate_blueprint(&blueprints[1], 24));
    let quality_level_sum = blueprints
        .par_iter()
        .map(|b| simulate_blueprint(b, 24).unwrap() * (b.id as u64))
        .sum::<u64>();
    println!("{}", quality_level_sum);
}
