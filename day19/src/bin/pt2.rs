use day19::*;

fn main() {
    let blueprints = read_data().into_iter().take(3).collect::<Vec<_>>();
    let geodes_prod = blueprints
        .par_iter()
        .map(|b| simulate_blueprint(b, 32).unwrap())
        .product::<u64>();
    println!("{}", geodes_prod);
    //println!("{:?}", simulate_blueprint(&blueprints[1], 32));
}
