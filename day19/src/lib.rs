use nom::{bytes::complete::tag, character::complete::u64 as nomu64, Finish, IResult};
use pathfinding::prelude::dijkstra;
use std::fs;

pub use rayon::prelude::*;

type Ore = u64;
type Clay = u64;
type Obsidian = u64;
type Resources = (Ore, Clay, Obsidian);

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
#[repr(usize)]
enum BotType {
    Ore = 0,
    Clay = 1,
    Obsidian = 2,
    Geode = 3,
}

#[derive(Debug)]
pub struct Blueprint {
    pub id: u8,
    costs: [Resources; 4],
}

// Time, Resources, Bots, Geodes, Don't buy, never buy
type Point = (u64, Resources, [u64; 4], u64, u8, u8);

pub fn simulate_blueprint(b: &Blueprint, time: u64) -> Option<u64> {
    const INV_COST: u64 = 10_000;

    let initial_point: Point = (1, (0, 0, 0), [1, 0, 0, 0], 0, 0, 0);
    let mut max_geodes_per_time = vec![0u64; time as usize + 1];
    let path = dijkstra(
        &initial_point,
        |p| {
            {
                // Eagerly reject paths that fall behind
                let t = p.0 as usize;
                let g = p.3;

                let max_g = max_geodes_per_time[t];

                if g < max_g.saturating_sub(2) {
                    return vec![];
                } else if g > max_g {
                    max_geodes_per_time[t] = g;
                }
            }
            use BotType::*;
            let t_left = time - p.0;
            if let Some(point) = try_buy_bot(p, b, &Some(Geode)) {
                vec![(point, INV_COST - t_left)]
            } else {
                let mut never_buy = p.5;

                let never_buy_obs = (never_buy & (1 << Obsidian as usize) != 0) || {
                    let g_cost = b.costs[Geode as usize].2;
                    // do you already have enough to buy 1 geode bot each minute?
                    t_left * g_cost <= p.1.2
                    // do you have enough bots to produce enough
                    || p.2[Obsidian as usize] >= g_cost
                };
                never_buy |= (never_buy_obs as u8) << Obsidian as usize;

                let never_buy_clay = (never_buy & (1 << Clay as usize) != 0) || {
                    let ob_cost = b.costs[Obsidian as usize].1;
                    // do you already have enough to buy 1 obs bot each minute?
                    t_left * ob_cost <= p.1.2
                    // do you have enough bots to produce enough
                    || p.2[Clay as usize] >= ob_cost
                };
                never_buy |= (never_buy_clay as u8) << Clay as usize;

                let never_buy_ore = (never_buy & (1 << Ore as usize) != 0) || {
                    let or_cost = b.costs[Ore as usize].0;
                    let cl_cost = b.costs[Clay as usize].0;
                    let ge_cost = b.costs[Geode as usize].0;
                    let max_cost = or_cost.max(cl_cost).max(ge_cost);

                    p.2[Ore as usize] >= max_cost
                };
                never_buy |= (never_buy_ore as u8) << Ore as usize;

                let could_buy = [
                    Some(Obsidian).filter(|_| {
                        (!never_buy_obs)
                        // did you choose to not buy one last time when you could?
                        && p.4 & (1 << Obsidian as usize) == 0
                    }),
                    Some(Clay).filter(|_| {
                        (!never_buy_clay)
                        // did you choose to not buy one last time when you could?
                        && p.4 & (1 << Clay as usize) == 0
                    }),
                    Some(Ore).filter(|_| {
                        (!never_buy_ore)
                        // did you choose to not buy one last time when you could?
                        && p.4 & (1 << Ore as usize) == 0
                    }),
                ]
                .par_iter()
                .flatten()
                .copied()
                .map(|bot| (try_buy_bot(p, b, &Some(bot)), INV_COST, bot))
                .filter_map(|(np, c, bot)| np.map(|np| (np, c, bot)))
                .collect::<Vec<_>>();
                let no_buy_point: (Point, _) = {
                    let mut point = try_buy_bot(p, b, &None).unwrap();
                    for bot in could_buy.iter().map(|(_np, _c, bot)| *bot) {
                        point.4 |= 1 << (bot as usize);
                    }
                    (point, INV_COST)
                };
                could_buy
                    .into_iter()
                    .map(|(mut np, c, _bot)| {
                        (
                            {
                                np.5 = never_buy;
                                np
                            },
                            c,
                        )
                    })
                    .chain([no_buy_point])
                    .collect()
            }
        },
        |(t, _r, _bots, _g, _, _)| {
            //println!("{}", t);
            *t >= time
        },
    )?;
    //println!("{:?}", path);
    Some((INV_COST * (time - 1)) - path.1)
}

fn try_buy_bot(p: &Point, b: &Blueprint, bot: &Option<BotType>) -> Option<Point> {
    // Take Resources
    let res = p.1;
    let res = if let Some(bot) = bot {
        (
            res.0.checked_sub(b.costs[*bot as usize].0)?,
            res.1.checked_sub(b.costs[*bot as usize].1)?,
            res.2.checked_sub(b.costs[*bot as usize].2)?,
        )
    } else {
        res
    };
    // Gather Resources
    let res = {
        (
            res.0 + p.2[BotType::Ore as usize],
            res.1 + p.2[BotType::Clay as usize],
            res.2 + p.2[BotType::Obsidian as usize],
        )
    };
    let geodes = p.3 + p.2[BotType::Geode as usize];
    // Add new bot
    let bots = match bot {
        None => p.2,
        Some(b) => {
            let mut new_bots = p.2;
            new_bots[*b as usize] += 1;
            new_bots
        }
    };
    Some((p.0 + 1, res, bots, geodes, 0, p.5))
}

fn blueprint(input: &str) -> IResult<&str, Blueprint> {
    let (input, id) = {
        let (input, _) = tag("Blueprint ")(input)?;
        let (input, id) = nomu64(input)?;
        let (input, _) = tag(": ")(input)?;
        (input, id)
    };
    let (input, ore_robot_cost) = {
        let (input, _) = tag("Each ore robot costs ")(input)?;
        let (input, ore) = nomu64(input)?;
        let (input, _) = tag(" ore. ")(input)?;
        (input, (ore, 0, 0))
    };
    let (input, clay_robot_cost) = {
        let (input, _) = tag("Each clay robot costs ")(input)?;
        let (input, ore) = nomu64(input)?;
        let (input, _) = tag(" ore. ")(input)?;
        (input, (ore, 0, 0))
    };
    let (input, obsidian_robot_cost) = {
        let (input, _) = tag("Each obsidian robot costs ")(input)?;
        let (input, ore) = nomu64(input)?;
        let (input, _) = tag(" ore and ")(input)?;
        let (input, clay) = nomu64(input)?;
        let (input, _) = tag(" clay. ")(input)?;
        (input, (ore, clay, 0))
    };
    let (input, geode_robot_cost) = {
        let (input, _) = tag("Each geode robot costs ")(input)?;
        let (input, ore) = nomu64(input)?;
        let (input, _) = tag(" ore and ")(input)?;
        let (input, obsidian) = nomu64(input)?;
        let (input, _) = tag(" obsidian.")(input)?;
        (input, (ore, 0, obsidian))
    };
    let costs = [
        ore_robot_cost,
        clay_robot_cost,
        obsidian_robot_cost,
        geode_robot_cost,
    ];
    Ok((
        input,
        Blueprint {
            id: id as u8,
            costs,
        },
    ))
}

pub fn read_data() -> Vec<Blueprint> {
    let text = fs::read_to_string("inputs/day19.txt").unwrap();
    text.lines()
        .collect::<Vec<&str>>()
        .par_iter()
        .filter_map(|l| blueprint(l).finish().ok().map(|(_input, b)| b))
        .collect()
}
