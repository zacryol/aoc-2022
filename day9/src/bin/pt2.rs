use std::collections::HashSet;

use day9::*;

fn main() {
    let mut rope = vec![(0, 0); 10];
    let mut tail_visited = HashSet::<(isize, isize)>::new();
    for d in read_data() {
        {
            move_head(&mut rope[0], d);
            for i in 1..10 {
                pull_tail(rope[i - 1], &mut rope[i]);
            }
        }
        tail_visited.insert(*rope.last().unwrap());
    }
    println!("{}", tail_visited.len());
}
