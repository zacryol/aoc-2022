use std::collections::HashSet;

use day9::*;

fn main() {
    let mut head = (0, 0);
    let mut tail = (0, 0);
    let mut tail_visited = HashSet::<(isize, isize)>::new();
    for d in read_data() {
        move_head(&mut head, d);
        pull_tail(head, &mut tail);
        tail_visited.insert(tail);
    }
    println!("{}", tail_visited.len());
}
