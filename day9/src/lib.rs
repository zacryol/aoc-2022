pub fn read_data() -> Vec<(isize, isize)> {
    std::fs::read_to_string("inputs/day9.txt")
        .unwrap()
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| l.split_once(' ').unwrap())
        .flat_map(|(dir, dist)| std::iter::repeat(dir).take(dist.parse().unwrap()))
        .map(|dir| match dir {
            "D" => (0, 1),
            "L" => (-1, 0),
            "R" => (1, 0),
            "U" => (0, -1),
            _ => (0, 0),
        })
        .collect()
}

pub fn move_head(h: &mut (isize, isize), dir: (isize, isize)) {
    h.0 += dir.0;
    h.1 += dir.1;
}

pub fn pull_tail(h: (isize, isize), t: &mut (isize, isize)) {
    match (t.0 - h.0, t.1 - h.1) {
        (-2, 0) => t.0 += 1,
        (2, 0) => t.0 -= 1,
        (0, 2) => t.1 -= 1,
        (0, -2) => t.1 += 1,
        (1, 2) | (2, 1) | (2, 2) => {
            t.0 -= 1;
            t.1 -= 1;
        }
        (-1, 2) | (-2, 1) | (-2, 2) => {
            t.0 += 1;
            t.1 -= 1;
        }
        (1, -2) | (2, -1) | (2, -2) => {
            t.0 -= 1;
            t.1 += 1;
        }
        (-1, -2) | (-2, -1) | (-2, -2) => {
            t.0 += 1;
            t.1 += 1;
        }
        (_x @ -1..=1, _y @ -1..=1) => {}
        (x, y) => eprintln!("Tail too far {:?}.", (x, y)),
    }
}
