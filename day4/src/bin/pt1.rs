fn main() {
    let text = std::fs::read_to_string("inputs/day4.txt").unwrap();
    let count = text
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| {
            l.split(',')
                .map(|h| h.split('-').map(str::parse::<usize>).map(Result::unwrap))
                .map(|mut h| (h.next().unwrap(), h.next().unwrap()))
                .collect::<Vec<(usize, usize)>>()
        })
        .filter(|l| {
            let r1 = l[0];
            let r2 = l[1];
            day4::range_contained(&r1, &r2) || day4::range_contained(&r2, &r1)
        })
        .count();
    println!("{}", count);
}
