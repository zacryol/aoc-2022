pub fn range_contained(outer: &(usize, usize), inner: &(usize, usize)) -> bool {
    outer.0 <= inner.0 && outer.1 >= inner.1
}

pub fn range_overlap(r1: &(usize, usize), r2: &(usize, usize)) -> bool {
    r1.1 >= r2.0 && r2.1 >= r1.0
}
