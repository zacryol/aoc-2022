use pathfinding::prelude::{astar, bfs}; // I choose to be lazy today

#[derive(Debug)]
pub struct Map(Vec<Vec<char>>);

impl Map {
    pub fn read() -> (Self, Point, Point) {
        let text = std::fs::read_to_string("inputs/day12.txt").unwrap();
        let mut map = Map(text
            .lines()
            .filter(|l| !l.is_empty())
            .map(|l| l.chars().collect())
            .collect());
        let mut start = Point { x: 0, y: 0 };
        let mut end = Point { x: 0, y: 0 };
        for y in 0..(map.0.len()) {
            for x in 0..(map.0[0].len()) {
                match map.0[y][x] {
                    'S' => {
                        start = Point { x, y };
                        map.0[y][x] = 'a';
                    }
                    'E' => {
                        end = Point { x, y };
                        map.0[y][x] = 'z';
                    }
                    _ => (),
                }
            }
        }

        (map, start, end)
    }

    pub fn point_neighbors(&self, p: Point, rev: bool) -> Vec<Point> {
        p.all_neighbors()
            .into_iter()
            .filter(|p| p.y < self.0.len() && p.x < self.0[0].len())
            .filter(|&Point { x, y }| match rev {
                false => self.0[y][x] <= (self.0[p.y][p.x] as u8 + 1) as char,
                true => self.0[p.y][p.x] <= (self.0[y][x] as u8 + 1) as char,
            })
            .collect()
    }

    pub fn len_x(&self) -> usize {
        self.0[0].len()
    }

    pub fn len_y(&self) -> usize {
        self.0.len()
    }

    pub fn point_is_lowest(&self, p: Point) -> bool {
        self.0[p.y][p.x] == 'a'
    }

    pub fn path_to_end(&self, start: Point, end: Point) -> Option<(Vec<Point>, usize)> {
        astar(
            &start,
            |p| self.point_neighbors(*p, false).into_iter().map(|p| (p, 1)),
            |p| p.dist(end),
            |p| *p == end,
        )
    }

    pub fn path_to_lowest(&self, start: Point) -> Option<Vec<Point>> {
        bfs(
            &start,
            |p| self.point_neighbors(*p, true),
            |p| self.point_is_lowest(*p),
        )
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub struct Point {
    pub x: usize,
    pub y: usize,
}

impl Point {
    pub fn dist(&self, other: Point) -> usize {
        self.x.abs_diff(other.x) + self.y.abs_diff(other.y)
    }

    fn all_neighbors(&self) -> Vec<Self> {
        let mut points = Vec::with_capacity(5);
        points.extend([
            Point {
                x: self.x,
                y: self.y + 1,
            },
            Point {
                x: self.x + 1,
                y: self.y,
            },
        ]);
        if self.x > 0 {
            points.extend([Point {
                x: self.x - 1,
                y: self.y,
            }])
        }
        if self.y > 0 {
            points.extend([Point {
                x: self.x,
                y: self.y - 1,
            }])
        }

        points
    }
}
