use day12::*;

fn main() {
    let (map, start, end) = Map::read();
    let result = map.path_to_end(start, end).unwrap();
    println!("{}", result.1);
}
