use day12::*;

fn main() {
    let (map, _, end) = Map::read();
    let shortest_path_len = map.path_to_lowest(end).unwrap().len() - 1; // -1 because path includes
                                                                        // start and end
    println!("{:?}", shortest_path_len);
}
