use pathfinding::prelude::dijkstra;
use std::fs;

type BlizzardMap = Vec<Vec<BlizzardSquare>>;

// Bitmask of blizzards present; 255 means a wall
type BlizzardSquare = u8;

enum BlizzardType {
    Up = 1,
    Down = 2,
    Left = 4,
    Right = 8,
}

// Time, (X, Y)
type Point = (usize, (usize, usize), usize);

pub fn find_blizzard_path(blizzards: &BlizzardMap, part2: bool) -> (Vec<Point>, usize) {
    let start = (blizzards[0].iter().position(|b| *b == 0).unwrap(), 0);
    let end = (
        blizzards
            .last()
            .unwrap()
            .iter()
            .position(|b| *b == 0)
            .unwrap(),
        blizzards.len() - 1,
    );
    let endv = if part2 { 3 } else { 0 };

    let mut blizzard_maps = vec![blizzards.clone()];

    dijkstra(
        &(0, start, 0),
        |(t, (x, y), v)| {
            let new_t = t + 1;
            let v = *v;
            let old_blizz = get_blizz_map_at(*t, &mut blizzard_maps);
            if old_blizz[*y][*x] != 0 {
                return vec![];
            }
            vec![
                ((new_t, (*x + 1, *y), v), 1),
                ((new_t, (*x - 1, *y), v), 1),
                {
                    let (x, y) = (*x, (y + 1).min(old_blizz.len() - 1));
                    let v = if v % 2 == 0 && (x, y) == end {
                        v + 1
                    } else {
                        v
                    };
                    ((new_t, (x, y), v), 1)
                },
                {
                    let (x, y) = (*x, y.saturating_sub(1));
                    let v = if v % 2 == 1 && (x, y) == start {
                        v + 1
                    } else {
                        v
                    };
                    ((new_t, (x, y), v), 1)
                },
                ((new_t, (*x, *y), v), 1),
            ]
        },
        |(_t, (x, y), v)| (*x, *y) == end && *v >= endv,
    )
    .unwrap()
}

fn get_blizz_map_at(t: usize, blizzard_maps: &mut Vec<BlizzardMap>) -> &BlizzardMap {
    while blizzard_maps.len() <= t {
        let new_b = next_blizzard_map(blizzard_maps.last().unwrap());
        blizzard_maps.push(new_b);
    }
    &blizzard_maps[t]
}

pub fn next_blizzard_map(blizzards: &BlizzardMap) -> BlizzardMap {
    (0..blizzards.len())
        .map(|y| {
            (0..blizzards[y].len())
                .map(|x| next_value(blizzards, (x, y)))
                .collect()
        })
        .collect()
}

pub fn blizz_print(blizzards: &BlizzardMap) {
    for row in blizzards.iter() {
        for point in row.iter() {
            print!(
                "{}",
                match *point {
                    0 => '.',
                    255 => '#',
                    _x if _x == BlizzardType::Up as u8 => '^',
                    _x if _x == BlizzardType::Down as u8 => 'v',
                    _x if _x == BlizzardType::Left as u8 => '<',
                    _x if _x == BlizzardType::Right as u8 => '>',
                    other => (other.count_ones() as u8 + b'0') as char,
                }
            )
        }
        println!();
    }
}

fn next_value(blizzards: &BlizzardMap, (x, y): (usize, usize)) -> u8 {
    if blizzards[y][x] == 255 {
        // Wall
        return 255;
    }
    if y == 0 || y == blizzards.len() - 1 {
        // Start or end point
        return 0;
    }
    assert_ne!(x, 0);
    let (xl, xr) = (
        if x == 1 {
            blizzards[y].len() - 2
        } else {
            x - 1
        },
        if x == blizzards[y].len() - 2 {
            1
        } else {
            x + 1
        },
    );
    let (yu, yd) = (
        if y == 1 { blizzards.len() - 2 } else { y - 1 },
        if y == blizzards.len() - 2 { 1 } else { y + 1 },
    );
    (blizzards[y][xl] & BlizzardType::Right as u8)
        | (blizzards[y][xr] & BlizzardType::Left as u8)
        | (blizzards[yu][x] & BlizzardType::Down as u8)
        | (blizzards[yd][x] & BlizzardType::Up as u8)
}

pub fn read_data() -> BlizzardMap {
    let text = fs::read_to_string("inputs/day24.txt").unwrap();
    text.lines()
        .filter(|l| !l.is_empty())
        .map(|l| {
            l.chars()
                .map(|c| match c {
                    '#' => 255,
                    '.' => 0,
                    'v' | 'V' => BlizzardType::Down as u8,
                    '^' => BlizzardType::Up as u8,
                    '>' => BlizzardType::Right as u8,
                    '<' => BlizzardType::Left as u8,
                    _ => panic!(),
                })
                .collect()
        })
        .collect()
}
