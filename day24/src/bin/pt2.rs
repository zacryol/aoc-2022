use day24::*;

fn main() {
    let blizz = read_data();
    println!("{:?}", find_blizzard_path(&blizz, true).1);
}
