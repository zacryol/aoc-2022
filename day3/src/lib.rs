use std::collections::HashSet;

pub fn get_overlap(rs: &str) -> Vec<char> {
    let (rs1, rs2) = rs.split_at(rs.len() / 2);
    let (rs1, rs2) = (
        rs1.chars().collect::<HashSet<_>>(),
        rs2.chars().collect::<HashSet<_>>(),
    );
    rs1.intersection(&rs2).copied().collect::<Vec<_>>()
}

pub fn get_priority(c: char) -> u8 {
    match c {
        c @ 'a'..='z' => c as u8 - 96,
        c @ 'A'..='Z' => c as u8 - 38,
        _ => panic!(),
    }
}
