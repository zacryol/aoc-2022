fn main() {
    let text = std::fs::read_to_string("inputs/day3.txt").unwrap();
    let priority_sum = text
        .lines()
        .map(|line| {
            day3::get_overlap(line)
                .into_iter()
                .map(day3::get_priority)
                .map(Into::<usize>::into)
                .sum::<usize>()
        })
        .sum::<usize>();
    println!("{}", priority_sum);
}
