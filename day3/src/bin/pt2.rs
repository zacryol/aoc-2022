use std::collections::HashSet;

fn main() {
    let text = std::fs::read_to_string("inputs/day3.txt").unwrap();
    let lines = text.lines().collect::<Vec<&str>>();
    let priority_sum = lines
        .chunks(3)
        .map(|group| {
            group
                .iter()
                .map(|l| l.chars().collect::<HashSet<_>>())
                .reduce(|hash, iter| {
                    hash.intersection(&iter.iter().copied().collect())
                        .copied()
                        .collect()
                })
                .unwrap()
                .into_iter()
                .map(day3::get_priority)
                .map(Into::<usize>::into)
                .sum::<usize>()
        })
        .sum::<usize>();
    println!("{}", priority_sum);
}
