use day14::*;

fn main() {
    let mut map = Map::build_initial(false);
    let count = (1..)
        .map(|i| map.drop_sand(Point { x: 500, y: 0 }).map(|p| (p, i)))
        //.inspect(|t| println!("{:?}", t))
        .take_while(|o| o.is_some())
        .count();
    println!("{}", count);
    //std::fs::write("end.txt", format!("{}", map)).unwrap();
}
