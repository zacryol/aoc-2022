use nom::{
    bytes::complete::tag, character::complete, multi::separated_list0, sequence::separated_pair,
    Finish, IResult,
};
use std::{collections::HashSet, fmt::Display, fs, iter::repeat};

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Square {
    Rock,
    Sand,
    Air,
}

impl Square {
    fn is_solid(&self) -> bool {
        *self != Self::Air
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub struct Point {
    pub x: u32,
    pub y: u32,
}

fn point(input: &str) -> IResult<&str, Point> {
    let (input, (x, y)) = separated_pair(complete::u32, tag(","), complete::u32)(input)?;
    Ok((input, Point { x, y }))
}

pub fn point_stream(input: &str) -> IResult<&str, Vec<Point>> {
    let (input, v) = separated_list0(tag(" -> "), point)(input)?;
    Ok((input, v))
}

fn get_points_between(p1: &Point, p2: &Point) -> Vec<Point> {
    match (p1, p2) {
        (pa, pb) if pa == pb => vec![],
        (Point { x: x1, y: y1 }, Point { x: x2, y: y2 }) if x1 == x2 => {
            let x_range = repeat(*x1);
            let y_range = (y1.min(y2) + 1)..*(y1.max(y2));
            x_range.zip(y_range).map(|(x, y)| Point { x, y }).collect()
        }
        (Point { x: x1, y: y1 }, Point { x: x2, y: y2 }) if y1 == y2 => {
            let y_range = repeat(*y1);
            let x_range = (x1.min(x2) + 1)..*(x1.max(x2));
            x_range.zip(y_range).map(|(x, y)| Point { x, y }).collect()
        }
        _ => panic!(),
    }
}

fn fill(corners: Vec<Point>) -> Vec<Point> {
    let mut corners = corners.into_iter().peekable();
    let mut points = vec![];
    loop {
        let Some(point) = corners.next() else {
            return points
        };
        let inner = corners
            .peek()
            .map_or_else(Vec::new, |p2| get_points_between(&point, p2));
        points.push(point);
        points.extend(inner);
    }
}

pub fn read_data() -> HashSet<Point> {
    let text = fs::read_to_string("inputs/day14.txt").unwrap();
    text.lines()
        .map(|l| {
            let (input, points) = point_stream(l).finish().unwrap();
            assert_eq!(input, "");
            points
        })
        .flat_map(fill)
        .collect()
}

pub struct Map(Vec<Vec<Square>>);

impl Map {
    pub fn build_initial(with_floor: bool) -> Self {
        let rocks = read_data();
        let width = rocks
            .iter()
            .map(|Point { x, y: _ }| *x as usize)
            .max()
            .unwrap()
            + 1000;
        let height = rocks
            .iter()
            .map(|Point { x: _, y }| *y as usize)
            .max()
            .unwrap()
            + 1;

        let mut map = Map(vec![vec![Square::Air; width]; height]);
        if with_floor {
            map.0.push(vec![Square::Air; width]);
            map.0.push(vec![Square::Rock; width]);
        }
        for point in rocks {
            *map.get_mut(&point).unwrap() = Square::Rock;
        }

        map
    }

    fn get(&self, p: &Point) -> Option<&Square> {
        self.0.get(p.y as usize)?.get(p.x as usize)
    }

    fn get_mut(&mut self, p: &Point) -> Option<&mut Square> {
        self.0.get_mut(p.y as usize)?.get_mut(p.x as usize)
    }

    pub fn drop_sand(&mut self, at: Point) -> Option<Point> {
        let mut sand_at = at;
        'sandmove: loop {
            let points_to_check = [
                Point {
                    x: sand_at.x,
                    y: sand_at.y + 1,
                },
                Point {
                    x: sand_at.x - 1,
                    y: sand_at.y + 1,
                },
                Point {
                    x: sand_at.x + 1,
                    y: sand_at.y + 1,
                },
            ];
            for p in points_to_check {
                if !self.get(&p)?.is_solid() {
                    sand_at = p;
                    continue 'sandmove;
                }
            }
            *self.get_mut(&sand_at)? = Square::Sand;
            return Some(sand_at).filter(|p| *p != at);
        }
    }
}

impl Display for Map {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            self.0
                .iter()
                .map(|l| l
                    .iter()
                    .map(|s| match s {
                        Square::Air => '.',
                        Square::Rock => '#',
                        Square::Sand => 'o',
                    })
                    .collect::<String>()
                    + "\n")
                .collect::<String>()
        )
    }
}
