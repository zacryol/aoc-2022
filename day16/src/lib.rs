use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{alpha1, u64 as nomu64},
    multi::separated_list1,
    Finish, IResult,
};
use pathfinding::prelude::dijkstra;
use std::{borrow::Cow, collections::HashSet, fs, rc::Rc};

#[derive(Debug, PartialEq, Eq, Hash, Ord, PartialOrd)]
pub struct Valve {
    pub name: Rc<String>,
    flow_rate: u64,
    connected_valves: Vec<Rc<String>>,
}

type ValvePre<'a> = (Cow<'a, str>, u64, Vec<Cow<'a, str>>);

fn valve(input: &str) -> IResult<&str, Cow<str>> {
    let (input, _) = tag("Valve ")(input)?;
    let (input, valve) = alpha1(input)?;
    Ok((input, Cow::Borrowed(valve)))
}

fn flow_rate(input: &str) -> IResult<&str, u64> {
    let (input, _) = tag(" has flow rate=")(input)?;
    let (input, rate) = nomu64(input)?;
    let (input, _) = tag(";")(input)?;
    Ok((input, rate))
}

fn connected_valves(input: &str) -> IResult<&str, Vec<Cow<str>>> {
    let (input, _) = alt((
        tag(" tunnels lead to valves "),
        tag(" tunnel leads to valve "),
    ))(input)?;
    let (input, v) = separated_list1(tag(", "), alpha1)(input)?;
    Ok((input, v.into_iter().map(Cow::Borrowed).collect()))
}

fn valve_data(input: &str) -> IResult<&str, ValvePre> {
    let (input, name) = valve(input)?;
    let (input, rate) = flow_rate(input)?;
    let (input, conn) = connected_valves(input)?;
    Ok((input, (name, rate, conn)))
}

pub fn read_data() -> Vec<Rc<Valve>> {
    let text = fs::read_to_string("inputs/day16.txt").unwrap();
    let valves_pre = text
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| valve_data(l).finish().unwrap().1)
        .collect::<Vec<_>>();
    let names = valves_pre
        .iter()
        .map(|v| Rc::new(v.0.clone().into_owned()))
        .collect::<HashSet<Rc<String>>>();
    let get_rc = |n: &str| Rc::clone(names.iter().find(|r| ***r == *n).unwrap());
    let mut valves = valves_pre
        .into_iter()
        .map(|(n, f, c)| {
            let name = get_rc(&n);
            let flow_rate = f;
            let connected_valves = c.into_iter().map(|v| get_rc(&v)).collect();
            Rc::new(Valve {
                name,
                flow_rate,
                connected_valves,
            })
        })
        .collect::<Vec<_>>();
    valves.sort();
    valves
}

type Point = (u64, Rc<Valve>, Rc<Vec<Rc<Valve>>>);
pub fn find_path(valves: &[Rc<Valve>]) -> Option<(Vec<Point>, u64)> {
    // Each point is represented spatially by the identifier, temporally by the current minute, and
    // multiversally by a hashset of valves that are already opened

    let starting_point: Point = {
        let first_valve = valves.iter().find(|v| *v.name == "AA").unwrap();

        (1, Rc::clone(first_valve), Rc::new(Vec::new()))
    };

    dijkstra(
        &starting_point,
        |p| {
            let t = p.0;
            let v = Rc::clone(&p.1);
            let vis = p.2.clone();
            let mut next_points = Vec::with_capacity(v.connected_valves.len() + 1);
            let next_t = t + 1;
            // Possibly open current valve
            if could_open_valve(&v, &vis) {
                next_points.push((
                    (next_t, Rc::clone(&v), {
                        let mut vis = Rc::clone(&vis);
                        Rc::make_mut(&mut vis).push(Rc::clone(&v));
                        Rc::make_mut(&mut vis).sort();
                        vis
                    }),
                    10000 - ((30 - t) * v.flow_rate),
                ));
            }
            for next_valve in v
                .connected_valves
                .iter()
                .flat_map(|nv| valves.iter().find(|vv| *vv.name == **nv))
            {
                next_points.push(((next_t, Rc::clone(next_valve), vis.clone()), 10000));
            }

            next_points
        },
        |(time, _, _)| *time >= 30,
    )
}

type ValveIdx = usize;
type Point2 = (u64, (ValveIdx, ValveIdx), usize, usize);

/// index, flow rate, connections
#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
struct ValveLight {
    idx: ValveIdx,
    flo: u64,
    con: usize,
}

pub fn find_path2(valves: &[Rc<Valve>]) -> Option<(Vec<Point2>, u64)> {
    let valves_light = valves
        .iter()
        .enumerate()
        .map(|(i, v)| ValveLight {
            idx: i,
            flo: v.flow_rate,
            con: v
                .connected_valves
                .iter()
                .map(|vv| 1 << get_valve_idx_by_name(valves, vv).unwrap())
                .reduce(|a, b| a | b)
                .unwrap(),
        })
        .collect::<Vec<ValveLight>>();
    // same as part1, but the Point contains 2 spatial valves; one for you and the elephant
    let starting_point: Point2 = {
        let first_valve = get_valve_idx_by_name(valves, "AA").unwrap();

        (1, (first_valve, first_valve), 0, 0)
    };

    dijkstra(
        &starting_point,
        |p| {
            let t = p.0;
            let you_valve = valves_light[p.1 .0];
            let ele_valve = valves_light[p.1 .1];
            let vis = p.2;
            let ignores = p.3;
            let next_t = t + 1;
            let you_next = get_possible_next(&you_valve, &valves_light, vis, ignores);
            let ele_next = get_possible_next(&ele_valve, &valves_light, vis, ignores);
            you_next
                .into_iter()
                .flat_map(|(yv, yg, yatv)| {
                    ele_next
                        .iter()
                        .filter_map(|(ev, eg, eatv)| {
                            // don't allow both opening same valve
                            if yv == *ev && (yg == *eg) {
                                return None;
                            }
                            let mut new_vis = vis;
                            let mut y_ignores = ignores;
                            let mut e_ignores = ignores;
                            if yatv {
                                new_vis |= 1 << yv.idx;
                                if (yv.con & !y_ignores).count_ones() <= 1 {
                                    y_ignores |= 1 << yv.idx;
                                };
                            }
                            if *eatv {
                                new_vis |= 1 << ev.idx;
                                if (ev.con & !e_ignores).count_ones() <= 1 {
                                    e_ignores |= 1 << ev.idx;
                                };
                            }
                            let y = yv;
                            let e = ev;
                            let first = y.min(*e);
                            let second = y.max(*e);

                            Some((
                                (
                                    next_t,
                                    (first.idx, second.idx),
                                    new_vis,
                                    y_ignores & e_ignores,
                                ),
                                10000 - ((26 - t) * (yg + eg)),
                            ))
                        })
                        .collect::<Vec<_>>()
                })
                .collect::<Vec<_>>()
        },
        |(time, _, _, _)| *time >= 26,
    )
}

fn get_possible_next(
    valve: &ValveLight,
    valves_light: &[ValveLight],
    vis: usize,
    ignores: usize,
) -> Vec<(ValveLight, u64, bool)> {
    let mut next = Vec::with_capacity(10);
    if could_open_valve2(valve, vis) {
        next.push((*valve, valve.flo, true));
    }
    for next_v in valves_light
        .iter()
        .filter(|vv| {
            // only valves that this one
            // connects to
            1 << vv.idx & valve.con != 0
        })
        .filter(|vv| (vv.con & !ignores).count_ones() > 1 || could_open_valve2(vv, vis))
    {
        next.push((*next_v, 0, false));
    }
    next
}

fn could_open_valve(valve: &Rc<Valve>, vis: &[Rc<Valve>]) -> bool {
    !vis.contains(valve) && valve.flow_rate > 0
}

fn could_open_valve2(valve: &ValveLight, vis: usize) -> bool {
    if valve.flo == 0 {
        return false;
    }
    (1 << valve.idx) & vis == 0
}

fn get_valve_idx_by_name(valves: &[Rc<Valve>], name: &str) -> Option<ValveIdx> {
    valves
        .binary_search_by_key(&name, |rc| rc.name.as_str())
        .ok()
}

/*fn get_valve_by_name(valves: &[Rc<Valve>], name: &str) -> Option<Rc<Valve>> {
    Some(Rc::clone(
        valves.index(get_valve_idx_by_name(&valves, &name)?),
    ))
}*/
