use day16::*;

fn main() {
    let valves = read_data();
    //println!("{:?}", find_path(&valves));
    let (_path_data, cost) = find_path2(&valves).unwrap();
    //println!("{:?}", path_data);
    println!("{}", 250000 - cost);
}
