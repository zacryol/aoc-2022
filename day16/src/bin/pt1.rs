use day16::*;

fn main() {
    let valves = read_data();
    //println!("{:?}", find_path(&valves));
    let (_path_data, cost) = find_path(&valves).unwrap();
    /*for point in path_data.iter() {
        //println!("{:?}", point);
        let vis = &point.2;
        print!("{}: ", point.0);
        for v in vis.iter() {
            print!("{:?} ", v.name);
        }
        println!("");
    }*/
    println!("{}", 290000 - cost);
}
