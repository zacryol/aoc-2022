use std::fs;

pub fn from_snafu(num: &str) -> u64 {
    num.chars().rev().enumerate().map(|(i, c)| {match c {
        '=' => -2,
        '-' => -1,
        '0' => 0,
        '1' => 1,
        '2' => 2,
        other => panic!("{}", other),
    }} * 5i64.pow(i as u32)).sum::<i64>() as u64
}

pub fn to_snafu(num: u64) -> String {
    let mut digits = {
        let digit_count = (0u32..)
            .map(|i| (i, 5u64.pow(i)))
            .find(|(_i, p)| *p > num)
            .unwrap()
            .0
            + 1;
        vec![0; digit_count as usize]
    };
    let mut tracker = num;
    digits.iter_mut().enumerate().rev().for_each(|(pow, dig)| {
        while tracker >= 5u64.pow(pow as u32) {
            *dig += 1;
            tracker -= 5u64.pow(pow as u32)
        }
    });
    let mut digits_2: Vec<_> = digits.iter().map(|n| *n as i64).collect();
    for i in 0..(digits_2.len() - 1) {
        if digits_2[i] > 2 {
            digits_2[i] -= 5;
            digits_2[i + 1] += 1;
        }
    }
    digits_2
        .into_iter()
        .rev()
        .skip_while(|d| *d == 0)
        .map(|d| match d {
            -2 => '=',
            -1 => '-',
            0 => '0',
            1 => '1',
            2 => '2',
            other => panic!("{}", other),
        })
        .collect()
}

pub fn read_data() -> Vec<u64> {
    let text = fs::read_to_string("inputs/day25.txt").unwrap();
    text.lines()
        .filter(|l| !l.is_empty())
        .map(from_snafu)
        .collect()
}
