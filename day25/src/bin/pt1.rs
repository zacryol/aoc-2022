use day25::*;

fn main() {
    println!("{}", to_snafu(read_data().into_iter().sum()));
}
