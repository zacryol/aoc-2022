use once_cell::sync::Lazy;
use std::{
    collections::{HashMap, HashSet, VecDeque},
    fs,
};

type ElfPos = (i32, i32);

#[derive(Debug, Clone, Copy)]
enum CheckDir {
    North,
    South,
    West,
    East,
}

impl CheckDir {
    fn get_check_points(&self, point: &ElfPos) -> [ElfPos; 3] {
        use CheckDir::*;
        let (x, y) = *point;
        match self {
            North => [(x - 1, y - 1), (x, y - 1), (x + 1, y - 1)],
            South => [(x - 1, y + 1), (x, y + 1), (x + 1, y + 1)],
            West => [(x - 1, y - 1), (x - 1, y), (x - 1, y + 1)],
            East => [(x + 1, y - 1), (x + 1, y), (x + 1, y + 1)],
        }
    }

    fn get_target_point(&self, point: &ElfPos) -> ElfPos {
        use CheckDir::*;
        let (x, y) = *point;
        match self {
            North => (x, y - 1),
            South => (x, y + 1),
            West => (x - 1, y),
            East => (x + 1, y),
        }
    }
}

pub fn empty_in_used_rect(points: &HashSet<ElfPos>) -> usize {
    let ((xmin, ymin), (xmax, ymax)) = calc_rect(points);
    let mut count = 0;
    for y in ymin..=ymax {
        for x in xmin..=xmax {
            if !points.contains(&(x, y)) {
                count += 1;
            }
        }
    }
    count
}

fn calc_rect(points: &HashSet<ElfPos>) -> (ElfPos, ElfPos) {
    points.iter().fold(
        ((i32::MAX, i32::MAX), (i32::MIN, i32::MIN)),
        |((xmin, ymin), (xmax, ymax)), (x, y)| {
            ((xmin.min(*x), ymin.min(*y)), (xmax.max(*x), ymax.max(*y)))
        },
    )
}

pub fn take_round(points: &mut HashSet<ElfPos>) -> bool {
    let moves = step1(points, false);
    //panic!();
    if moves.is_empty() {
        false
    } else {
        step2(points, moves);
        true
    }
}

// HashMap has move-to point as key, and move-from point as value
fn step1(points: &HashSet<ElfPos>, read_only: bool) -> HashMap<ElfPos, ElfPos> {
    static mut CHECK_PRIORITY: Lazy<VecDeque<CheckDir>> = Lazy::new(|| {
        VecDeque::from([
            CheckDir::North,
            CheckDir::South,
            CheckDir::West,
            CheckDir::East,
        ])
    });
    let mut targetings: HashMap<ElfPos, Option<ElfPos>> = HashMap::new();
    points
        .iter()
        .map(|p| {
            (
                p,
                unsafe { CHECK_PRIORITY.iter().copied() }
                    .map(|d| {
                        (
                            d.get_check_points(p).iter().all(|tp| !points.contains(tp)),
                            d,
                        )
                    })
                    .collect::<Vec<_>>(),
            )
        })
        .filter(|(_p, mb)| !mb.iter().all(|(can, _d)| *can))
        .filter(|(_p, mb)| mb.iter().any(|(can, _d)| *can))
        .for_each(|(from_point, movability)| {
            //println!("{:?}", (&from_point, &movability));
            let target_point = {
                let move_dir = movability.into_iter().find(|(c, _d)| *c).unwrap().1;
                //println!("{:?}", move_dir);
                move_dir.get_target_point(from_point)
            };
            match targetings.get_mut(&target_point) {
                Some(None) => {}
                Some(o) => *o = None,
                None => {
                    targetings.insert(target_point, Some(*from_point));
                }
            }
        });
    targetings.retain(|_, v| v.is_some());
    if !read_only {
        unsafe {
            CHECK_PRIORITY.rotate_left(1);
        }
    }
    targetings
        .into_iter()
        .map(|(k, v)| (k, v.unwrap()))
        .collect()
}

fn step2(points: &mut HashSet<ElfPos>, moves: HashMap</*to*/ ElfPos, /*from*/ ElfPos>) {
    for (to, from) in moves.into_iter() {
        points.remove(&from);
        points.insert(to);
    }
}

pub fn print_points(points: &HashSet<ElfPos>) {
    for y in 0..=15 {
        for x in 0..=15 {
            print!("{}", if points.contains(&(x, y)) { '#' } else { '.' });
        }
        println!();
    }
    println!("===============");
}

pub fn read_data() -> HashSet<ElfPos> {
    let text = fs::read_to_string("inputs/day23.txt").unwrap();
    text.lines()
        .enumerate()
        .flat_map(|(y, l)| {
            l.chars()
                .enumerate()
                .map(move |(x, c)| ((x as i32, y as i32), c))
        })
        .filter_map(|((x, y), c)| if c == '#' { Some((x, y)) } else { None })
        .collect()
}
