use day23::*;

fn main() {
    let mut data = read_data();
    for _i in 0..10 {
        take_round(&mut data);
    }
    println!("{}", empty_in_used_rect(&data));
}
