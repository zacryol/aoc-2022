use day23::*;
use std::iter::repeat_with;

fn main() {
    let mut data = read_data();
    let rounds = repeat_with(|| take_round(&mut data))
        .take_while(|m| *m)
        .count();
    println!("{}", rounds + 1);
}
