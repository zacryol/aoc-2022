use day17::*;

fn main() {
    println!("{}", Cavern::emulate_n_rows(1_000_000_000_000));
}
