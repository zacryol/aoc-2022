use day17::*;

fn main() {
    let mut cave = Cavern::new();
    //println!("{}", cave);
    for _ in 0..2022 {
        cave.drop_next_rock();
    }
    println!("{}", cave.get_rocks_height());
    //println!("{}", cave);
}
