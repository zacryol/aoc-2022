use std::{
    fmt::{self, Debug, Display},
    fs,
    iter::{Cycle, Enumerate, Peekable},
    vec::IntoIter,
};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub enum RockType {
    Horizontal,
    Plus,
    ReverseL,
    Vertical,
    Square,
}

impl RockType {
    fn get_char(&self) -> char {
        use RockType::*;
        match self {
            Horizontal => 'H',
            Plus => 'P',
            ReverseL => 'L',
            Vertical => 'V',
            Square => 'S',
        }
    }

    fn get_shape(&self) -> &'static [[bool; 4]; 4] {
        use RockType::*;
        match self {
            Horizontal => &[[true; 4], [false; 4], [false; 4], [false; 4]],
            Plus => &[
                [false, true, false, false],
                [true, true, true, false],
                [false, true, false, false],
                [false; 4],
            ],
            ReverseL => &[
                [true, true, true, false],
                [false, false, true, false],
                [false, false, true, false],
                [false; 4],
            ],
            Vertical => &[[true, false, false, false]; 4],
            Square => &[
                [true, true, false, false],
                [true, true, false, false],
                [false; 4],
                [false; 4],
            ],
        }
    }
}

struct RockCycle(RockType);

impl Default for RockCycle {
    // contains the previously sent rock
    // defaults to Square so the next one sent will be Horizontal
    fn default() -> Self {
        Self(RockType::Square)
    }
}

impl Iterator for RockCycle {
    type Item = RockType;

    fn next(&mut self) -> Option<Self::Item> {
        use RockType::*;
        self.0 = match self.0 {
            Horizontal => Plus,
            Plus => ReverseL,
            ReverseL => Vertical,
            Vertical => Square,
            Square => Horizontal,
        };
        Some(self.0)
    }
}

fn read_data() -> Vec<i8> {
    let text = fs::read_to_string("inputs/day17.txt").unwrap();
    text.chars()
        .filter_map(|c| match c {
            '<' => Some(-1),
            '>' => Some(1),
            _ => None,
        })
        .collect()
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum PointStatus {
    Air,
    FallingRock,
    RestingRock(RockType),
}

pub struct Cavern(
    Vec<[PointStatus; 7]>,
    Peekable<Cycle<Enumerate<IntoIter<i8>>>>,
    Peekable<RockCycle>,
);

impl Debug for Cavern {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Cavern")
    }
}

impl Default for Cavern {
    fn default() -> Self {
        Self::new()
    }
}

impl Cavern {
    pub fn new() -> Self {
        Self(
            vec![[PointStatus::Air; 7]; 1000],
            read_data().into_iter().enumerate().cycle().peekable(),
            RockCycle::default().peekable(),
        )
    }

    pub fn emulate_n_rows(n: usize) -> usize {
        let (mut cave, rows_per_cycle, rocks_per_cycle, mut total_rocks_dropped) =
            Self::find_cycle();
        let mut fake_height = cave.get_rocks_height();
        // Fake k cycles
        let k = (n - total_rocks_dropped) / rocks_per_cycle;
        fake_height += rows_per_cycle * k;
        total_rocks_dropped += rocks_per_cycle * k;
        while total_rocks_dropped < n {
            let old_real_height = cave.get_rocks_height();
            total_rocks_dropped += 1;
            cave.drop_next_rock();
            fake_height += cave.get_rocks_height() - old_real_height;
        }
        fake_height
    }

    pub fn get_rocks_height(&self) -> usize {
        self.0
            .iter()
            .enumerate()
            .find(|(_i, r)| **r == [PointStatus::Air; 7])
            .unwrap()
            .0
    }

    pub fn find_cycle() -> (Cavern, usize, usize, usize) {
        let mut cave = Self::new();
        let mut total_rocks_dropped = 0;
        macro_rules! drop_rock {
            () => {{
                total_rocks_dropped += 1;
                cave.drop_next_rock()
            }};
        }
        let mut max_fall = (0..50).map(|_i| drop_rock!().1).max().unwrap();
        //println!("{}", max_fall);
        let cycle_len_rows = 'cycle: loop {
            max_fall = max_fall.max(drop_rock!().1);
            let cycle_search_start = cave.get_rocks_height() - max_fall;
            for n in 10..(cycle_search_start / 2 - 5) {
                let two = &cave.0[(cycle_search_start - n * 2)..cycle_search_start];
                let first = &two[0..n];
                let second = &two[n..];
                if first == second {
                    //println!("{:?}", first.len());
                    break 'cycle first.len();
                }
            }
        };
        // count rocks for one more cycle
        let mut rocks_per_cycle = 0;
        let old_height = cave.get_rocks_height();
        while cave.get_rocks_height() < old_height + cycle_len_rows {
            drop_rock!();
            rocks_per_cycle += 1;
        }
        //println!("{}", rocks_per_cycle);
        //println!("{}", total_rocks_dropped);
        (cave, cycle_len_rows, rocks_per_cycle, total_rocks_dropped)
    }

    pub fn drop_next_rock(&mut self) -> (RockType, usize) {
        let next_rock = self.2.next().unwrap();
        (next_rock, self.drop_rock(next_rock))
    }

    pub fn drop_rock(&mut self, rock: RockType) -> usize {
        let mut distance_fallen = 0;
        if self.get_rocks_height() + 20 > self.0.len() {
            self.0.append(&mut vec![[PointStatus::Air; 7]; 100]);
        }
        let spawn_position = (2usize, self.get_rocks_height() + 3);
        let rock_data = rock.get_shape();
        let mut rock_positions = Vec::with_capacity(5);
        for x in 0..4 {
            for (y, rock_row) in rock_data.iter().enumerate().take(4) {
                let pos = (spawn_position.0 + x, spawn_position.1 + y);
                if rock_row[x] {
                    //self.0[pos.1][pos.0] = PointStatus::FallingRock;
                    rock_positions.push(pos);
                }
            }
        }
        //println!("{}", self);

        loop {
            let dir = self.1.next().unwrap().1;
            let try_new = rock_positions
                .iter()
                .map(|(x, y)| (x.wrapping_add_signed(dir as isize), *y))
                .collect::<Vec<_>>();
            if try_new
                .iter()
                .map(|(x, y)| self.0.get(*y).and_then(|r| r.get(*x)))
                .all(|o| o == Some(&PointStatus::Air))
            {
                rock_positions = try_new;
            };

            let try_new = rock_positions
                .iter()
                .map(|(x, y)| (*x, y.wrapping_add_signed(-1)))
                .collect::<Vec<_>>();
            if try_new
                .iter()
                .map(|(x, y)| self.0.get(*y).and_then(|r| r.get(*x)))
                .all(|o| o == Some(&PointStatus::Air))
            {
                rock_positions = try_new;
                distance_fallen += 1;
            } else {
                rock_positions.iter().for_each(|(x, y)| {
                    self.0[*y][*x] = PointStatus::RestingRock(rock);
                });
                return distance_fallen;
            };
        }
    }
}

impl Display for Cavern {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for row in (0..(self.get_rocks_height() + 10)).rev() {
            writeln!(
                f,
                "{:>8} -- |{}|",
                row,
                self.0[row]
                    .iter()
                    .map(|p| match p {
                        PointStatus::Air => '.',
                        PointStatus::FallingRock => '@',
                        PointStatus::RestingRock(r) => r.get_char(),
                    })
                    .collect::<String>()
            )?
        }
        write!(f, "            +-------+")
    }
}
