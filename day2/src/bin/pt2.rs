use std::str::FromStr;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
#[repr(u8)]
enum Rps {
    Rock = 1,
    Paper = 2,
    Scissors = 3,
}

impl FromStr for Rps {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "A" => Self::Rock,
            "B" => Self::Paper,
            "C" => Self::Scissors,
            _ => panic!(),
        })
    }
}

impl Rps {
    /// X => lose, Y => draw, Z => win
    fn get_counter(&self, outcome: &str) -> Self {
        use Rps::*;
        match (self, outcome) {
            (_, "Y") => *self,
            (Rock, "Z") | (Scissors, "X") => Paper,
            (Rock, "X") | (Paper, "Z") => Scissors,
            (Paper, "X") | (Scissors, "Z") => Rock,
            _ => unimplemented!(),
        }
    }

    fn raw_score(&self) -> usize {
        *self as u8 as usize
    }

    fn get_match_score(&self, other: &Self) -> usize {
        use Rps::*;
        match (self, other) {
            (x, y) if x == y => 3,
            (Rock, Scissors) | (Scissors, Paper) | (Paper, Rock) => 6,
            _ => 0,
        }
    }
}

fn main() {
    let score = std::fs::read_to_string("inputs/day2.txt")
        .unwrap()
        .lines()
        .filter(|s| !s.is_empty())
        .map(|line| {
            let (other, outcome) = line.split_once(' ').unwrap();
            let other = other.parse::<Rps>().unwrap();
            let you = other.get_counter(outcome);
            you.raw_score() + you.get_match_score(&other)
        })
        .sum::<usize>();
    println!("{}", score);
}
