use std::str::FromStr;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
#[repr(u8)]
enum Rps {
    Rock = 1,
    Paper = 2,
    Scissors = 3,
}

impl FromStr for Rps {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "A" | "X" => Self::Rock,
            "B" | "Y" => Self::Paper,
            "C" | "Z" => Self::Scissors,
            _ => panic!(),
        })
    }
}

impl Rps {
    fn raw_score(&self) -> usize {
        *self as u8 as usize
    }

    fn get_match_score(&self, other: &Self) -> usize {
        use Rps::*;
        match (self, other) {
            (x, y) if x == y => 3,
            (Rock, Scissors) | (Scissors, Paper) | (Paper, Rock) => 6,
            _ => 0,
        }
    }
}

fn main() {
    let text = std::fs::read_to_string("inputs/day2.txt").unwrap();
    let score = text
        .lines()
        .filter(|s| !s.is_empty())
        .map(|line| {
            let (other, you) = line.split_once(' ').unwrap();
            let (other, you) = (other.parse::<Rps>().unwrap(), you.parse::<Rps>().unwrap());
            you.raw_score() + you.get_match_score(&other)
        })
        .sum::<usize>();
    println!("{}", score);
}
