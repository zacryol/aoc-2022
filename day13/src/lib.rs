use nom::{
    branch::alt, bytes::complete::tag, character::complete, multi::separated_list0, Finish, IResult,
};
use std::{borrow::Cow, cmp::Ordering, fs};

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum PacketToken {
    Num(u32),
    List(Vec<PacketToken>),
}

impl PacketToken {
    fn as_list(&self) -> Cow<Vec<Self>> {
        use PacketToken::{List, Num};
        match self {
            List(l) => Cow::Borrowed(l),
            Num(n) => Cow::Owned(vec![Self::Num(*n)]),
        }
    }
}

impl Ord for PacketToken {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl PartialOrd for PacketToken {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        use PacketToken::Num;
        match (self, other) {
            (Num(n1), Num(n2)) => n1.partial_cmp(n2),
            (p1, p2) => {
                let v1 = p1.as_list();
                let v2 = p2.as_list();
                let mut i1 = v1.iter();
                let mut i2 = v2.iter();
                'cmp: loop {
                    let (a, b) = (i1.next(), i2.next());
                    match (a, b) {
                        (Some(_), None) => break 'cmp Some(Ordering::Greater),
                        (None, Some(_)) => break 'cmp Some(Ordering::Less),
                        (Some(c), Some(d)) => match c.partial_cmp(d) {
                            Some(Ordering::Less) => break 'cmp Some(Ordering::Less),
                            Some(Ordering::Greater) => break 'cmp Some(Ordering::Greater),
                            Some(Ordering::Equal) => continue 'cmp,
                            None => break 'cmp None,
                        },
                        _ => break 'cmp Some(Ordering::Equal),
                    }
                }
            }
        }
    }
}

fn number(input: &str) -> IResult<&str, PacketToken> {
    let (input, n) = complete::u32(input)?;
    Ok((input, PacketToken::Num(n)))
}

fn list(input: &str) -> IResult<&str, PacketToken> {
    let (input, _) = tag("[")(input)?;
    let (input, vec) = separated_list0(tag(","), token)(input)?;
    let (input, _) = tag("]")(input)?;

    Ok((input, PacketToken::List(vec)))
}

pub fn token(input: &str) -> IResult<&str, PacketToken> {
    alt((number, list))(input)
}

pub fn read_packet_list() -> Vec<PacketToken> {
    let text = fs::read_to_string("inputs/day13.txt").unwrap();
    text.lines()
        .filter(|l| !l.is_empty())
        .map(token)
        .map(|r| r.finish().unwrap())
        .map(|(input, tok)| {
            assert_eq!(input, "");
            tok
        })
        .collect()
}

pub fn packet_pairs(v: &[PacketToken]) -> impl Iterator<Item = (&PacketToken, &PacketToken)> {
    v.chunks_exact(2).map(|s| (&s[0], &s[1]))
}
