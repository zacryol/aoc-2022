use day13::*;
use nom::Finish;

fn main() {
    let mut packets = read_packet_list();
    let div1 = token("[[2]]").finish().unwrap().1;
    let div2 = token("[[6]]").finish().unwrap().1;
    packets.extend([div1.clone(), div2.clone()]);
    packets.sort();
    let idx1 = packets.binary_search(&div1).unwrap() + 1;
    let idx2 = packets.binary_search(&div2).unwrap() + 1;
    println!("{}", idx1 * idx2);
}
