use day13::*;

fn main() {
    let packets = read_packet_list();
    let in_order_id_sum = packet_pairs(&packets)
        .zip(1..)
        .filter(|((p1, p2), _i)| p1 < p2)
        .map(|((_, _), i)| i)
        .sum::<usize>();
    println!("{}", in_order_id_sum);
}
