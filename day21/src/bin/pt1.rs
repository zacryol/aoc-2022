use day21::*;

fn main() {
    let monkeys = read_data();
    println!("{:?}", calc_monkey("root", &monkeys));
}
