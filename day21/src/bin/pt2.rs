use day21::*;

fn main() {
    let (monkeys, n1, n2) = {
        let mut monkeys = read_data();
        let (n1, n2) = fix_monkeys(&mut monkeys);
        (monkeys, n1, n2)
    };
    let nums = make_cache(&monkeys);
    let humn_val = if monkeys[&n1].needs_humn(&monkeys) {
        force_value(&n1, &monkeys, &nums, nums[n2.as_str()])
    } else {
        force_value(&n2, &monkeys, &nums, nums[n1.as_str()])
    };
    println!("{}", humn_val);
}
