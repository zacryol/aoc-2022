use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{alpha1, i64 as nomi64, one_of},
    Finish, IResult,
};
use rayon::prelude::*;
use std::{
    collections::HashMap,
    fs,
    ops::{Add, Div, Mul, Sub},
};

#[derive(Debug, Clone)]
pub struct Monkey {
    name: String,
    job: MonkeyJob,
}

impl Monkey {
    pub fn needs_humn(&self, monkeys: &HashMap<String, Monkey>) -> bool {
        match &self.job {
            MonkeyJob::Num(_) => false,
            MonkeyJob::Op(n1, _, n2) if n1 == "humn" || n2 == "humn" => true,
            MonkeyJob::Op(n1, _, n2) => {
                monkeys[n1].needs_humn(monkeys) || monkeys[n2].needs_humn(monkeys)
            }
        }
    }

    // WORKS FOR ME
    #[allow(clippy::fn_address_comparisons)]
    pub fn num1_to_get(&self, value: i64, num2: i64) -> Option<i64> {
        let MonkeyJob::Op(_, f, _) = self.job else {
            return None
        };
        Some(if f == i64::add {
            value - num2
        } else if f == i64::sub {
            value + num2
        } else if f == i64::mul {
            value / num2
        } else if f == i64::div {
            value * num2
        } else {
            return None;
        })
    }

    #[allow(clippy::fn_address_comparisons)]
    pub fn num2_to_get(&self, value: i64, num1: i64) -> Option<i64> {
        let MonkeyJob::Op(_, f, _) = self.job else {
            return None
        };
        Some(if f == i64::add {
            value - num1
        } else if f == i64::sub {
            num1 - value
        } else if f == i64::mul {
            value / num1
        } else if f == i64::div {
            num1 / value
        } else {
            return None;
        })
    }
}

#[derive(Debug, Clone)]
enum MonkeyJob {
    Num(i64),
    Op(String, fn(i64, i64) -> i64, String),
}

pub fn read_data() -> HashMap<String, Monkey> {
    let text = fs::read_to_string("inputs/day21.txt").unwrap();
    text.lines()
        .collect::<Vec<&str>>()
        .par_iter()
        .filter_map(|l| {
            monkey(l).finish().ok().map(|(input, m)| {
                assert_eq!(input, "");
                (m.name.clone(), m)
            })
        })
        .collect()
}

/// Make changes for part 2
pub fn fix_monkeys(monkeys: &mut HashMap<String, Monkey>) -> (String, String) {
    //monkeys.remove("humn");
    let root = monkeys.remove("root").unwrap();
    let MonkeyJob::Op(n1, _, n2) = root.job else {
        panic!();
    };
    (n1, n2)
}

pub fn make_cache(monkeys: &HashMap<String, Monkey>) -> HashMap<&str, i64> {
    monkeys
        .iter()
        .filter_map(|(n, m)| {
            if m.needs_humn(monkeys) {
                None
            } else {
                Some((n.as_str(), calc_monkey(n, monkeys)))
            }
        })
        .collect()
}

pub fn force_value(
    name: &str,
    monkeys: &HashMap<String, Monkey>,
    nums: &HashMap<&str, i64>,
    value: i64,
) -> i64 {
    //println!("Monkey {name} must have value {value}");
    if name == "humn" {
        return value;
    }
    assert!(monkeys[name].needs_humn(monkeys));
    let (n1, n2) = match &monkeys[name].job {
        MonkeyJob::Num(_) => panic!(),
        MonkeyJob::Op(n1, _, n2) => (n1.as_str(), n2.as_str()),
    };
    if n1 == "humn" {
        force_value(
            "humn",
            monkeys,
            nums,
            monkeys[name].num1_to_get(value, nums[n2]).unwrap(),
        )
    } else if n2 == "humn" {
        todo!("base case 2");
    } else if monkeys[n1].needs_humn(monkeys) {
        force_value(
            n1,
            monkeys,
            nums,
            monkeys[name].num1_to_get(value, nums[n2]).unwrap(),
        )
    } else if monkeys[n2].needs_humn(monkeys) {
        force_value(
            n2,
            monkeys,
            nums,
            monkeys[name].num2_to_get(value, nums[n1]).unwrap(),
        )
    } else {
        panic!()
    }
}

pub fn calc_monkey(name: &str, monkeys: &HashMap<String, Monkey>) -> i64 {
    match &monkeys[name].job {
        MonkeyJob::Num(n) => *n,
        MonkeyJob::Op(n1, f, n2) => f(calc_monkey(n1, monkeys), calc_monkey(n2, monkeys)),
    }
}

fn monkey(input: &str) -> IResult<&str, Monkey> {
    let (input, name) = alpha1(input)?;
    let (input, _) = tag(": ")(input)?;
    let (input, job) = alt((monkey_op, monkey_num))(input)?;
    Ok((
        input,
        Monkey {
            name: name.to_owned(),
            job,
        },
    ))
}

fn monkey_num(input: &str) -> IResult<&str, MonkeyJob> {
    let (input, num) = nomi64(input)?;
    Ok((input, MonkeyJob::Num(num)))
}

fn monkey_op(input: &str) -> IResult<&str, MonkeyJob> {
    let (input, name1) = alpha1(input)?;
    let (input, _) = tag(" ")(input)?;
    let (input, op) = one_of("+-*/")(input)?;
    let op = match op {
        '+' => i64::add,
        '-' => i64::sub,
        '*' => i64::mul,
        '/' => i64::div,
        _ => unreachable!(),
    };
    let (input, _) = tag(" ")(input)?;
    let (input, name2) = alpha1(input)?;
    Ok((input, MonkeyJob::Op(name1.to_owned(), op, name2.to_owned())))
}
