use nom::{bytes::complete::tag, character::complete, Finish, IResult};
use std::{collections::HashSet, fs, ops::RangeInclusive};

type Pos = (i64, i64);
fn xy_pair(input: &str) -> IResult<&str, Pos> {
    let (input, _) = tag("x=")(input)?;
    let (input, x) = complete::i64(input)?;
    let (input, _) = tag(", y=")(input)?;
    let (input, y) = complete::i64(input)?;
    Ok((input, (x, y)))
}

pub fn sensor_beacon(input: &str) -> IResult<&str, (Pos, Pos)> {
    let (input, _) = tag("Sensor at ")(input)?;
    let (input, sensor_pos) = xy_pair(input)?;
    let (input, _) = tag(": closest beacon is at ")(input)?;
    let (input, beacon_pos) = xy_pair(input)?;
    Ok((input, (sensor_pos, beacon_pos)))
}

pub fn read_data() -> Vec<(Pos, Pos)> {
    let text = fs::read_to_string("inputs/day15.txt").unwrap();
    text.lines()
        .filter(|l| !l.is_empty())
        .map(|l| {
            let (text, pair) = sensor_beacon(l).finish().unwrap();
            assert_eq!(text, "");
            pair
        })
        .collect()
}

// Pt1 brute force
pub fn excluded_points(
    sensor: &Pos,
    beacon: &Pos,
    restrict_row: Option<RangeInclusive<i64>>,
    restrict_column: Option<RangeInclusive<i64>>,
) -> HashSet<Pos> {
    let x_diff = sensor.0.abs_diff(beacon.0);
    let y_diff = sensor.1.abs_diff(beacon.1);
    let total_diff = x_diff + y_diff;

    let x_range = {
        let bounds = {
            let x_min = sensor.0 - (total_diff as i64);
            let x_max = sensor.0 + (total_diff as i64);
            x_min..=x_max
        };

        restrict_column.map_or_else(
            || bounds.clone(),
            |r| *(bounds.start().max(r.start()))..=*(bounds.end().min(r.end())),
        )
    };

    let y_range = {
        let bounds = {
            let y_min = sensor.1 - (total_diff as i64);
            let y_max = sensor.1 + (total_diff as i64);
            y_min..=y_max
        };

        restrict_row.map_or_else(
            || bounds.clone(),
            |r| *(bounds.start().max(r.start()))..=*(bounds.end().min(r.end())),
        )
    };

    x_range
        .flat_map(|x| y_range.clone().map(move |y| (x, y)))
        .filter(|(x, y)| {
            let x_diff = sensor.0.abs_diff(*x);
            let y_diff = sensor.1.abs_diff(*y);
            x_diff + y_diff <= total_diff
        })
        .filter(|p| p != beacon)
        .collect::<HashSet<_>>()
}

pub fn get_just_outside_points(sensor: &Pos, beacon: &Pos) -> Vec<Pos> {
    let just_outside_dist = {
        let x_diff = sensor.0.abs_diff(beacon.0);
        let y_diff = sensor.1.abs_diff(beacon.1);
        let total_diff = x_diff + y_diff;
        (total_diff + 1) as i64
    };
    let mut points = Vec::with_capacity(just_outside_dist as usize * 4 + 4);
    let mut walk_pos = (0, just_outside_dist);
    loop {
        points.push(walk_pos);
        walk_pos = match walk_pos {
            (x, y) if x >= 0 && y > 0 => (x + 1, y - 1),
            (x, y) if y <= 0 && x > 0 => (x - 1, y - 1),
            (x, y) if x <= 0 && y < 0 => (x - 1, y + 1),
            (x, y) if y >= 0 && x < 0 => (x + 1, y + 1),
            _ => todo!(),
        };
        if walk_pos == (0, just_outside_dist) {
            break;
        }
    }

    points.iter_mut().for_each(|(x, y)| {
        *x += sensor.0;
        *y += sensor.1;
    });
    points
}

pub fn point_in_sensor_range(point: &Pos, sensor: &Pos, beacon: &Pos) -> bool {
    let total_diff = {
        let x_diff = sensor.0.abs_diff(beacon.0);
        let y_diff = sensor.1.abs_diff(beacon.1);
        x_diff + y_diff
    };
    let x_diff = sensor.0.abs_diff(point.0);
    let y_diff = sensor.1.abs_diff(point.1);
    x_diff + y_diff <= total_diff
}
