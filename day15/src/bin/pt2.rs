use day15::*;

fn main() {
    let pairs = read_data();
    let max = 4_000_000;

    let (x, y) = pairs
        .iter()
        .flat_map(|(s, b)| get_just_outside_points(s, b))
        .filter(|(x, y)| (0..=max).contains(x) && (0..=max).contains(y))
        .find(|p| !pairs.iter().any(|(s, b)| point_in_sensor_range(p, s, b)))
        .unwrap();
    println!("{:?}", (x * 4_000_000) + y);
}
