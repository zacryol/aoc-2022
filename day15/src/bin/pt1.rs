use day15::*;
use std::collections::HashSet;

fn main() {
    let pairs = read_data();
    let blocked_count = pairs
        .iter()
        .flat_map(|(s, b)| excluded_points(s, b, Some(2_000_000..=2_000_000), None))
        .collect::<HashSet<_>>()
        .len();
    println!("{}", blocked_count);
}
