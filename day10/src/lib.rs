pub fn x_values(input: &'_ str) -> impl Iterator<Item = (i32, i32)> + '_ {
    input
        .lines()
        .filter(|l| !l.trim().is_empty())
        .map(|l| l.strip_prefix("addx ").map(|s| s.parse::<i32>().unwrap()))
        .flat_map(|op| op.map_or_else(|| vec![0], |i| vec![0, i]))
        .map(|diff| {
            static mut ACCUMULATOR: i32 = 1;
            unsafe {
                ACCUMULATOR += diff;
                ACCUMULATOR
            }
        })
        .zip(2..)
}

pub const SCREEN_WIDTH: usize = 40;
pub const SCREEN_HEIGHT: usize = 6;

pub fn print_screen_packed(screen: &[char]) {
    screen
        .chunks(SCREEN_WIDTH)
        .for_each(|l| println!("{}", l.iter().collect::<String>()))
}
