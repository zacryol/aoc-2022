use day10::*;

fn main() {
    let text = std::fs::read_to_string("inputs/day10.txt").unwrap();
    let sum_at_points = x_values(&text)
        .filter(|(_s, i)| i % 40 == 20)
        .map(|(s, i)| s * i)
        .sum::<i32>();
    println!("{}", sum_at_points);
}
