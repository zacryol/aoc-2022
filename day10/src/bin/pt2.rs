use day10::*;

fn main() {
    let mut screen = vec![' '; SCREEN_WIDTH * SCREEN_HEIGHT];
    let text = std::fs::read_to_string("inputs/day10.txt").unwrap();

    for (x, i) in x_values(&text) {
        if (x - (i - 1) % (SCREEN_WIDTH as i32)).abs() < 2 {
            screen[i as usize] = '#';
        }
    }

    print_screen_packed(&screen);
}
