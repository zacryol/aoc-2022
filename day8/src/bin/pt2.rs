use day8::*;
use std::iter::repeat;

fn main() {
    let data = read_data();
    let max_score = (0..(data.len()))
        .flat_map(|i| repeat(i).zip(0..(data[0].len())))
        .map(|t| get_scenic_score(t, &data))
        .max()
        .unwrap();
    println!("{}", max_score);
}
