use std::sync::{Arc, RwLock};

use day8::*;

fn main() {
    let data = Arc::new(RwLock::new(read_data()));
    let vis_data = get_visibility(Arc::clone(&data));
    let vis_count = vis_data.iter().flatten().filter(|x| **x).count();
    println!("{}", vis_count);
}
