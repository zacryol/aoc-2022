use std::sync::{Arc, RwLock};
use std::thread::spawn;
pub type Forest = Vec<Vec<u8>>;

pub fn read_data() -> Forest {
    std::fs::read_to_string("inputs/day8.txt")
        .unwrap()
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| l.trim().chars().map(|c| c as u8 - 48).collect())
        .collect()
}

fn get_vis_march_up(f: Arc<RwLock<Forest>>) -> Vec<Vec<bool>> {
    let f = f.read().unwrap();
    let mut vis_data = vec![vec![false; f[0].len()]; f.len() - 1];
    vis_data.push(vec![true; f[0].len()]);

    for y in (0..(f.len() - 1)).rev() {
        (0..(f[y].len()))
            .filter(|x| !vis_data[y][*x])
            .collect::<Vec<_>>()
            .into_iter()
            .for_each(|x| {
                let b = ((y + 1)..(f.len()))
                    .take_while(|yy| f[*yy][x] < f[y][x])
                    .any(|yy| vis_data[yy][x]);
                vis_data[y][x] = b;
            });
    }
    vis_data
}

fn get_vis_march_down(f: Arc<RwLock<Forest>>) -> Vec<Vec<bool>> {
    let f = f.read().unwrap();
    let mut vis_data = vec![vec![false; f[0].len()]; f.len()];
    vis_data[0].iter_mut().for_each(|b| *b = true);

    for y in 1..(f.len()) {
        (0..(f[y].len()))
            .filter(|x| !vis_data[y][*x])
            .collect::<Vec<_>>()
            .into_iter()
            .for_each(|x| {
                let b = (0..y)
                    .rev()
                    .take_while(|yy| f[*yy][x] < f[y][x])
                    .any(|yy| vis_data[yy][x]);
                vis_data[y][x] = b;
            });
    }
    vis_data
}

fn get_vis_march_left(f: Arc<RwLock<Forest>>) -> Vec<Vec<bool>> {
    let f = f.read().unwrap();
    let mut vis_data = vec![vec![false; f[0].len()]; f.len()];
    vis_data
        .iter_mut()
        .for_each(|v| *v.last_mut().unwrap() = true);

    for x in (0..(f[0].len() - 1)).rev() {
        for y in 0..(f.len()) {
            let b = ((x + 1)..(f[0].len()))
                .take_while(|xx| f[y][*xx] < f[y][x])
                .any(|xx| vis_data[y][xx]);
            vis_data[y][x] = b;
        }
    }
    vis_data
}

fn get_vis_march_right(f: Arc<RwLock<Forest>>) -> Vec<Vec<bool>> {
    let f = f.read().unwrap();
    let mut vis_data = vec![vec![false; f[0].len()]; f.len()];
    vis_data.iter_mut().for_each(|v| v[0] = true);

    for x in 1..(f[0].len()) {
        for y in 0..(f.len()) {
            let b = (0..x)
                .rev()
                .take_while(|xx| f[y][*xx] < f[y][x])
                .any(|xx| vis_data[y][xx]);
            vis_data[y][x] = b;
        }
    }
    vis_data
}

pub fn get_visibility(f: Arc<RwLock<Forest>>) -> Vec<Vec<bool>> {
    let right = {
        let f = Arc::clone(&f);
        spawn(move || get_vis_march_right(f))
    };
    let left = {
        let f = Arc::clone(&f);
        spawn(move || get_vis_march_left(f))
    };
    let up = {
        let f = Arc::clone(&f);
        spawn(move || get_vis_march_up(f))
    };
    let down = {
        let f = Arc::clone(&f);
        spawn(move || get_vis_march_down(f))
    };

    let right = right.join().unwrap();
    let left = left.join().unwrap();
    let up = up.join().unwrap();
    let down = down.join().unwrap();

    right
        .iter()
        .zip(left.iter())
        .zip(up.iter())
        .zip(down.iter())
        .map(|(((a, b), c), d)| (a, b, c, d))
        .map(|(a, b, c, d)| {
            (0..(a.len()))
                .map(|i| a[i] || b[i] || c[i] || d[i])
                .collect::<Vec<_>>()
        })
        .collect()
}

pub fn get_scenic_score((ty, tx): (usize, usize), f: &Forest) -> usize {
    let th = f[ty][tx];
    let right_score = {
        ((tx + 1)..(f[0].len()))
            .enumerate()
            .skip_while(|(_i, x)| f[ty][*x] < th)
            .map(|(i, _)| i + 1)
            .next()
            .unwrap_or_else(|| f[0].len() - tx - 1)
    };
    let left_score = {
        (0..tx)
            .rev()
            .enumerate()
            .skip_while(|(_i, x)| f[ty][*x] < th)
            .map(|(i, _)| i + 1)
            .next()
            .unwrap_or(tx)
    };
    let up_score = {
        (0..ty)
            .rev()
            .enumerate()
            .skip_while(|(_i, y)| f[*y][tx] < th)
            .map(|(i, _)| i + 1)
            .next()
            .unwrap_or(ty)
    };
    let down_score = {
        ((ty + 1)..(f.len()))
            .enumerate()
            .skip_while(|(_i, y)| f[*y][tx] < th)
            .map(|(i, _)| i + 1)
            .next()
            .unwrap_or(f.len() - ty - 1)
    };
    up_score * down_score * left_score * right_score
}
