use day22::*;

fn main() {
    let (maze, instr) = read_data();
    let mut walker = Walker::new(&maze);
    for r#move in instr {
        walker.do_move(r#move, &maze, false);
    }
    println!("{}", walker.get_password());
}
