use nom::{
    branch::alt,
    character::complete::{one_of, u8 as nomu8},
    multi::many0,
    Finish, IResult,
};
use std::fs;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Tile {
    Open,
    Wall,
    Null,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Move {
    Forward(u8),
    TurnLeft,
    TurnRight,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum Dir {
    U = 3,
    D = 1,
    L = 2,
    R = 0,
}

impl Dir {
    fn turned_right(self) -> Self {
        match self {
            Dir::R => Dir::D,
            Dir::D => Dir::L,
            Dir::L => Dir::U,
            Dir::U => Dir::R,
        }
    }

    fn turned_left(self) -> Self {
        match self {
            Dir::R => Dir::U,
            Dir::D => Dir::R,
            Dir::L => Dir::D,
            Dir::U => Dir::L,
        }
    }
}

pub struct Walker {
    pos: (usize, usize),
    dir: Dir,
}

impl Walker {
    pub fn new(maze: &Maze) -> Self {
        Self {
            pos: (maze[0].iter().position(|t| *t == Tile::Open).unwrap(), 0),
            dir: Dir::R,
        }
    }

    pub fn do_move(&mut self, r#move: Move, maze: &Maze, cubed: bool) {
        //println!("{:?}", self.pos);
        //println!("{:?}", self.dir);
        //println!("{:?}", r#move);
        match r#move {
            Move::TurnRight => self.dir = self.dir.turned_right(),
            Move::TurnLeft => self.dir = self.dir.turned_left(),
            Move::Forward(n) => {
                for _ in 0..n {
                    if !self.try_fwd(maze, cubed) {
                        break;
                    }
                }
            }
        }
    }

    pub fn get_password(&self) -> usize {
        //println!("{:?}", self.pos);
        let row = self.pos.1 + 1;
        let col = self.pos.0 + 1;
        1000 * row + 4 * col + (self.dir as usize)
    }

    fn try_fwd(&mut self, maze: &Maze, cubed: bool) -> bool {
        let (pos, d) = if cubed {
            //cube_wrap_sample_input(self.pos, self.dir)
            cube_wrap_real_input(self.pos, self.dir)
        } else {
            (self.get_next_pos(maze), self.dir)
        };
        assert_ne!(check_maze_pos(maze, pos), Tile::Null);
        if check_maze_pos(maze, pos) == Tile::Open {
            self.pos = pos;
            self.dir = d;
            true
        } else {
            false
        }
    }

    fn get_next_pos(&self, maze: &Maze) -> (usize, usize) {
        let (x, y) = self.pos;
        let (x, y) = Self::move_point((x, y), self.dir);
        let target_tile = check_maze_pos(maze, (x, y));
        if target_tile != Tile::Null {
            (x, y)
        } else {
            Self::wrap_pos((x, y), self.dir, maze)
        }
    }

    fn wrap_pos((mut x, mut y): (usize, usize), d: Dir, maze: &Maze) -> (usize, usize) {
        assert_eq!(check_maze_pos(maze, (x, y)), Tile::Null);
        while check_maze_pos(maze, (x, y)) == Tile::Null {
            //println!("{:?}", (x, y));
            (x, y) = Self::move_point((x, y), d);
            if y >= maze.len() && d == Dir::D {
                y = 0;
            } else {
                y = y.min(maze.len() - 1);
            }
            if x >= maze[y].len() && d == Dir::R {
                x = 0;
            } else {
                x = x.min(maze[y].len() - 1);
            }
        }
        (x, y)
    }

    fn move_point((x, y): (usize, usize), d: Dir) -> (usize, usize) {
        match d {
            Dir::R => (x + 1, y),
            Dir::L => (x.wrapping_sub(1), y),
            Dir::U => (x, y.wrapping_sub(1)),
            Dir::D => (x, y + 1),
        }
    }
}

type Maze = Vec<Vec<Tile>>;

// Screw it, I'm hardcoding this
#[allow(dead_code)]
fn cube_wrap_sample_input(p: (usize, usize), d: Dir) -> ((usize, usize), Dir) {
    match (p.0, p.1, d) {
        (11, y @ 4..=7, Dir::R) => ((19 - y, 8), Dir::D), // face 4 => face 6
        (x @ 12..=15, 8, Dir::U) => ((11, 19 - x), Dir::L), // face 6 => face 4
        (x @ 4..=7, 4, Dir::U) => ((8, x - 4), Dir::R),   // face 3 => face 1
        (8, y @ 0..=3, Dir::L) => ((y + 4, 4), Dir::D),   // face 1 => face 3
        (x @ 4..=7, 7, Dir::D) => ((8, 15 - x), Dir::R),  // face 3 => face 5
        (8, y @ 8..=11, Dir::L) => ((15 - y, 7), Dir::U), // face 5 => face 3
        (x @ 0..=3, 7, Dir::D) => ((11 - x, 11), Dir::U), // face 2 => face 5
        (x @ 8..=11, 11, Dir::D) => ((11 - x, 7), Dir::U), // face 5 => face 2
        (x @ 8..=11, 0, Dir::U) => ((11 - x, 4), Dir::D), // face 1 => face 2
        (x @ 0..=3, 4, Dir::U) => ((11 - x, 0), Dir::D),  // face 2 => face 1
        (11, y @ 0..=3, Dir::R) => ((15, 15 - y), Dir::L), // face 1 => face 6
        (15, y @ 8..=11, Dir::R) => ((11, 15 - y), Dir::L), // face 6 => face 1
        (0, y @ 4..=7, Dir::L) => ((19 - y, 11), Dir::U), // face 2 => face 6
        (x @ 12..=15, 11, Dir::D) => ((0, 19 - x), Dir::R), // face 6 => face 2
        (x, y, d) => (Walker::move_point((x, y), d), d),  // still within map bounds
    }
}

// My input looks like (1/10 scale)
//      1111122222
//      1111122222
//      1111122222
//      1111122222
//      1111122222
//      33333
//      33333
//      33333
//      33333
//      33333
// 4444455555
// 4444455555
// 4444455555
// 4444455555
// 4444455555
// 66666
// 66666
// 66666
// 66666
// 66666
fn cube_wrap_real_input(p: (usize, usize), d: Dir) -> ((usize, usize), Dir) {
    match (p.0, p.1, d) {
        (99, y @ 50..=99, Dir::R) => ((y + 50, 49), Dir::U), // face 3 => face 2
        (x @ 100..=149, 49, Dir::D) => ((99, x - 50), Dir::L), // face 2 => face 3
        (50, y @ 50..=99, Dir::L) => ((y - 50, 100), Dir::D), // face 3 => face 4
        (x @ 0..=49, 100, Dir::U) => ((50, x + 50), Dir::R), // face 4 => face 3
        (x @ 50..=99, 149, Dir::D) => ((49, x + 100), Dir::L), // face 5 => face 6
        (49, y @ 150..=199, Dir::R) => ((y - 100, 149), Dir::U), // face 6 => face 5
        (x @ 50..=99, 0, Dir::U) => ((0, x + 100), Dir::R),  // face 1 => face 6
        (0, y @ 150..=199, Dir::L) => ((y - 100, 0), Dir::D), // face 6 => face 1
        (50, y @ 0..=49, Dir::L) => ((0, 149 - y), Dir::R),  // face 1 => face 4
        (0, y @ 100..=149, Dir::L) => ((50, 149 - y), Dir::R), // face 4 => face 1
        (149, y @ 0..=49, Dir::R) => ((99, 149 - y), Dir::L), // face 2 => face 5
        (99, y @ 100..=149, Dir::R) => ((149, 149 - y), Dir::L), // face 5 => face 2
        (x @ 100..=149, 0, Dir::U) => ((x - 100, 199), Dir::U), // face 2 => face 6
        (x @ 0..=49, 199, Dir::D) => ((x + 100, 0), Dir::D), // face 6 => face 2
        (x, y, d) => (Walker::move_point((x, y), d), d),     // still within map bounds
    }
}

fn check_maze_pos(maze: &Maze, (x, y): (usize, usize)) -> Tile {
    maze.get(y)
        .and_then(|row| row.get(x))
        .copied()
        .unwrap_or(Tile::Null)
}

pub fn read_data() -> (Maze, Vec<Move>) {
    #[allow(unused_variables)]
    let text = r#"        ...#
        .#..
        #...
        ....
...#.......#
........#...
..#....#....
..........#.
        ...#....
        .....#..
        .#......
        ......#.

10R5L5R10L4R5L5"#;
    let text = fs::read_to_string("inputs/day22.txt").unwrap();
    let mut lines = text.lines();
    let maze_lines: Vec<Vec<Tile>> = lines
        .by_ref()
        .take_while(|l| !l.is_empty())
        .map(|l| {
            l.chars()
                .filter_map(|c| match c {
                    ' ' => Some(Tile::Null),
                    '.' => Some(Tile::Open),
                    '#' => Some(Tile::Wall),
                    _ => None,
                })
                .collect()
        })
        .collect();
    let instructions_line = lines.find(|l| !l.is_empty()).unwrap();
    let instructions = many0(instruction)(instructions_line).finish().unwrap().1;
    (maze_lines, instructions)
}

fn instruction(input: &str) -> IResult<&str, Move> {
    alt((turn, fwd))(input)
}

fn turn(input: &str) -> IResult<&str, Move> {
    let (input, dir) = one_of("LR")(input)?;
    let r#move = match dir {
        'L' => Move::TurnLeft,
        'R' => Move::TurnRight,
        _ => unreachable!(),
    };
    Ok((input, r#move))
}

fn fwd(input: &str) -> IResult<&str, Move> {
    let (input, n) = nomu8(input)?;
    Ok((input, Move::Forward(n)))
}
