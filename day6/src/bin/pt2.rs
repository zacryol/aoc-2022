use std::collections::HashSet;

fn main() {
    let text = std::fs::read_to_string("inputs/day6.txt").unwrap();
    let index = (14..(text.len()))
        .map(|i| (i, &text[(i - 14)..i]))
        .find(|(_idx, txt)| txt.chars().collect::<HashSet<_>>().len() == 14);
    println!("{:?}", index);
}
