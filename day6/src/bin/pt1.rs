use std::collections::HashSet;

fn main() {
    let text = std::fs::read_to_string("inputs/day6.txt").unwrap();
    let unique_seq = (4..(text.len()))
        .map(|i| (i, &text[(i - 4)..i]))
        .find(|(_idx, txt)| txt.chars().collect::<HashSet<_>>().len() == 4);
    println!("{:?}", unique_seq);
}
