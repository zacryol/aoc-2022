use std::rc::Rc;
use std::str::FromStr;
#[derive(Debug)]
pub enum LineType {
    CDCommand(String),
    LSCommand,
    LSEntry { data: FieldType, name: String },
}

impl FromStr for LineType {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some(cd) = s.trim().strip_prefix("$ cd ") {
            Ok(Self::CDCommand(cd.to_string()))
        } else if s.trim() == "$ ls" {
            Ok(Self::LSCommand)
        } else if let Some((data, name)) = s.trim().split_once(' ') {
            Ok(Self::LSEntry {
                data: data.parse().map_or(FieldType::Dir, FieldType::File),
                name: name.to_string(),
            })
        } else {
            Err(())
        }
    }
}

#[derive(Debug)]
pub enum FieldType {
    Dir,
    File(usize),
}

#[derive(Debug)]
pub struct File {
    pub dir: Rc<String>,
    pub _name: String,
    pub size: usize,
}

pub fn get_fs_data() -> (Vec<File>, Vec<Rc<String>>) {
    let text = std::fs::read_to_string("inputs/day7.txt").unwrap();

    let mut walk_path = Vec::<String>::new();
    let mut files = Vec::<File>::new();
    let mut dirs = vec![Rc::new("/".to_string())];

    for line in text
        .lines()
        .filter(|l| !l.is_empty())
        .map(|s| s.parse::<LineType>().unwrap())
    {
        match line {
            LineType::CDCommand(cd) if cd == ".." => {
                walk_path.pop().unwrap();
            }
            LineType::CDCommand(cd) => walk_path.push(cd),
            LineType::LSEntry {
                data: FieldType::Dir,
                name,
            } => dirs.push(Rc::new(walk_path.join("/") + "/" + &name)),
            LineType::LSEntry {
                data: FieldType::File(size),
                name,
            } => files.push(File {
                size,
                _name: name,
                dir: Rc::clone(
                    dirs.iter()
                        .find(|d| d.split('/').skip(2).eq(walk_path.iter().skip(1)))
                        .unwrap(),
                ),
            }),
            _ => (),
        }
    }
    (files, dirs)
}

pub fn get_dir_size(files: &[File], d: &str) -> usize {
    files
        .iter()
        .filter(|f| f.dir.starts_with(d))
        .map(|f| f.size)
        .sum()
}
