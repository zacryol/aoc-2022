use day7::*;

fn main() {
    const TOTAL_AVAILABLE_SPACE: usize = 70000000;
    const NEEDED_UNUSED_SPACE: usize = 30000000;
    let (files, dirs) = get_fs_data();
    let total_used = get_dir_size(&files, "/");
    let current_available = TOTAL_AVAILABLE_SPACE - total_used;
    let need_to_free = NEEDED_UNUSED_SPACE - current_available;

    let freesize = dirs
        .iter()
        .map(|d| get_dir_size(&files, d))
        .filter(|ds| *ds >= need_to_free)
        .min()
        .unwrap();
    println!("{}", freesize);
}
