use day7::*;

fn main() {
    let (files, dirs) = get_fs_data();

    let size: usize = dirs
        .iter()
        .map(|d| get_dir_size(&files, d))
        .filter(|s| *s < 100000)
        .sum();
    println!("{}", size);
}
